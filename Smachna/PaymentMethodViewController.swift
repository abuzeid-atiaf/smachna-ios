//
//  PaymentMethodViewController.swift
//  Smachna
//
//  Created by Mac on 12/8/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class PaymentMethodViewController: BaseViewController {
    
    
    //var delegate: GeneralCallbackDelegate?
    var prams:String?
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        self.view.isOpaque = false
        
        
        
        //        self.preferredContentSize = CGSize(width:100,height:100)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBOutlet weak var knetOutlet: UIView!
    @IBOutlet weak var ondelveryOutlet: UIView!
    
    
    
    
    
    @IBAction func cachAction(_ sender: Any) {
        
        ondelveryOutlet.backgroundColor = UIColor.lightGray
        //delegate?.callbackObject(obj: "cach" as AnyObject)
        self.orderNow(type: "cach")
        
        
        
    }
    
    @IBAction func kentAction(_ sender: Any) {
        knetOutlet.backgroundColor = UIColor.lightGray
        //delegate?.callbackObject(obj: "kent" as AnyObject)
        self.orderNow(type: "knet")
    }
    
    func orderNow(type:String){
        
        //if app.isUserLoggedIn() {
        self.sendOrderToServer(prameters: prams! + "&payment_type=\(type)")
        //        }else{
        //
        //        let vc:ContinueSignupViewController = Cuts.vc(storyid: cids.continueSignupViewController) as! ContinueSignupViewController
        //        prams = prams! +  "&payment_type=\(type)"
        //
        //
        //
        //        vc.prameters = prams!
        //        vc.controllerTask = constants.flag_task_order
        //
        //        self.present(vc, animated: true, completion: nil)
        //
        //        }
        
        
        
        
    }
    @IBAction func dism2(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dimsis(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet var dismiss: UITapGestureRecognizer!
    func sendOrderToServer(prameters:String){
        Progress.show(sender: self);
        RequestManager.post(actionName: "home/add_order", params: prameters , respo: {(dictionary) -> Void in
            Progress.hide()
            if let userLogged = dictionary["success"]  {
                
                
                if ("\(userLogged)"  == "true"  ){
                    Toast.showSuccess()
                    app.removeCartItems()
                    Toast.alert(vc: self, message: "Order sent successfully".localized)
                    DispatchQueue.main.async{
                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2) ) {
                            Cuts.present(vc: self, storyid:cids.rootNavigationCtrl)
                        }
                        
                    }
                }else{
                    
                    DispatchQueue.main.async{
                        Toast.showFail()
                    }
                    
                    
                }
                
                
                
            }else{
                
                Toast.alert(message: "Can't complete your order".localized)
            }
        })
        
    }
}
