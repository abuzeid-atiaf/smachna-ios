//
//  ViewController.swift
//  Smachna
//
//  Created by Mac on 12/6/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getDataFromServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getCurrentordersFromServer()
    }
    func getCurrentordersFromServer(){
        
        
        if app.isUserLoggedIn(){
            
        let uid = app.getUserData()!.userid
        RequestManager.post(actionName: "home/client_orders", params: "user_id=\(uid)"  , respo: {(dictionary) -> Void in
            
            
            if let orders = dictionary["orders"] as?  [[String:AnyObject]] {
                
                app.setCurrentOrdersCount(count:"\(orders.count)")
                
            }
            }
        )
        
        }
    }
    
    
    func getDataFromServer(){
        
        
        
        RequestManager.post(actionName: "home/about", params: "", respo: {(dictionary) -> Void in
            Progress.hide()
            
            
                if let about = dictionary["about"] as?  [String:AnyObject] {
                    if let image = about["ads_img"] as? String {
                        UserDefaults.standard.set(image, forKey: "ads_image")
                        UserDefaults.standard.synchronize()
                        
                        
                    }
                    
                    
                }
            
            
            
            
        })
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
            
        case "english_segue":
            app.changeLnagToEn()
                LanguageManager.sharedInstance.setLocale(constants.englishCode)
            
            break;
        case "arabic_segue":
           app.changeLangToAr()
           LanguageManager.sharedInstance.setLocale(constants.arabicCode)

            break;
            
            
        default:
            break;
        }

    }
   

}

