//
//  SpinnerItemSelectedProtocol.swift
//  Super Market
//
//  Created by mac on 11/30/16.
//  Copyright © 2016 mac. All rights reserved.
//

import Foundation

@objc protocol SpinnerItemSelectedProtocol {
     @objc optional func itemSelected(selectedItem:[String:AnyObject]);
    
    @objc optional func callbackString(string:String);
}
