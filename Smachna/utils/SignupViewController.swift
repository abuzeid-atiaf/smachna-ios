//
//  SignupViewController.swift
//  Super Market
//
//  Created by Abuzeid on 11/25/16.
//  Copyright © 2016 mac. All rights reserved.
//

import UIKit
import FirebaseInstanceID


class SignupViewController: BaseViewController   ,UIPopoverPresentationControllerDelegate  {
    
    var controllerTask = 0;
    var prameters:String =  ""
    
          @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passField: UITextField!
    @IBOutlet weak var cpassField: UITextField!
    @IBOutlet weak var mobileField: UITextField!
    
    
    
    
    
    
    
    
    
        func adaptivePresentationStyleForPresentationController(PC: UIPresentationController!) -> UIModalPresentationStyle {
        // This *forces* a popover to be displayed on the iPhone
        return .none
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
 self.setNavgationBar(hasMenu: true,title: nil, subTitle: nil, showSearch: false, back: true, cart: false)
    
        self.backHome = cids.rootNavigationCtrl
        hideKeypadWithTextFields()
        
            self.scrollLayout.contentSize = CGSize(width: self.view.frame.size.width, height: 750)
        
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBOutlet var scrollLayout: UIScrollView!

    
    
 
    @IBAction func registerButton(_ sender: AnyObject) {
       
        self.view.endEditing(true)
        
        if Utils.textFieldsNotEmpty(tf: self.usernameField,self.emailField,passField,cpassField){
            
            
            if(passField.text! != cpassField.text!){
            
                Toast.alert(vc:self ,message: "password not identical".localized)
                return;
            }
            
            
            
            if Utils.isValidEmail(emailStr: emailField.text!) == false {
                Toast.alert(vc:self ,message: "please enter correct email".localized)
                return;
            }
            
            prameters += "devicetoken=" + FIRInstanceID.instanceID().token()!
            prameters += "&devicetype=" + constants.DEVICE_TYPE
            
            
            prameters += "&username=" + usernameField.text!
            prameters += "&mobile=" + mobileField.text!
            prameters += "&password=" + passField.text!
            prameters += "&email=" + emailField.text!
            
           
            let popoverMenuViewController:AlertContinueViewController = Cuts.vc(storyid: cids.alertContinueViewController) as! AlertContinueViewController
            
            
            popoverMenuViewController.parameters = prameters;
            popoverMenuViewController.modalPresentationStyle = .overCurrentContext //presentation style
            present(popoverMenuViewController, animated: true, completion: nil)
            popoverMenuViewController.popoverPresentationController?.sourceView = view
            popoverMenuViewController.popoverPresentationController?.sourceRect = sender.frame
            
            
            
        }else{
        
            Toast.alert(vc:self ,message: "check your inputs".localized)
        }
        
        
        
        
        
    }
}
