//
//  OverCustomizableView.swift
//  Super Market
//
//  Created by mac on 11/20/16.
//  Copyright © 2016 mac. All rights reserved.
//

import UIKit

class OverCustomizableView: UIView {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //addSubviews()
    }
    
    
    /*============*/
    @IBInspectable var bgImage: UIImage? {
        didSet {
            
            
            backgroundColor =  UIColor(patternImage:bgImage!)
            
        }
    }
    
    @IBInspectable var circleView: Bool = false {
        didSet {
            
            if circleView == true {
                
                layer.cornerRadius = frame.size.width / 2
                clipsToBounds = true
                
            }    }
    }
    
        
    
    /*============*/
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    /*============*/
    @IBInspectable var completeCircle: Bool = false {
        didSet {
            if(completeCircle){
                layer.cornerRadius = self.frame.size.width/2
                layer.masksToBounds = true
            }}
    }
    
    /*============*/
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    
    /*============*/
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor!.cgColor
        }
    }
    
    
    
    
    
}
