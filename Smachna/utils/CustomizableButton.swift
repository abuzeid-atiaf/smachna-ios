//
//  CustomizableButton.swift
//  Super Market
//
//  Created by mac on 11/22/16.
//  Copyright © 2016 mac. All rights reserved.
//

import UIKit

class CustomizableButton: UIButton {

    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    /*============*/
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    /*============*/
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
   
}

private var extrasKey:Any?

private var indexP:IndexPath?
private var val = "0"


extension UIButton{


    var extras: Any! {
        get {
            return objc_getAssociatedObject(self, &extrasKey)
        }
        set(newValue) {
            objc_setAssociatedObject(self, &extrasKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }

    
    var indexPath: IndexPath! {
        get {
            return objc_getAssociatedObject(self, &indexP) as! IndexPath!
        }
        set(newValue) {
            objc_setAssociatedObject(self, &indexP, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }

    
    var value: String! {
        get {
            return objc_getAssociatedObject(self, &val) as! String!
        }
        set(newValue) {
            objc_setAssociatedObject(self, &val, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }



}
