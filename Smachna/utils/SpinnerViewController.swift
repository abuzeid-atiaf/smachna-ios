//
//  SpinnerViewController.swift
//  Super Market
//
//  Created by mac on 11/30/16.
//  Copyright © 2016 mac. All rights reserved.
//

import UIKit
import Kingfisher
class SpinnerViewController:UIViewController , UITableViewDelegate ,UITableViewDataSource {
    
    @IBAction func cancelAction(_ sender: Any) {
        
        
        self.dismiss(animated: true, completion: nil)
    }
    
    private let reuseIdentifier  = "cell_spinner"
    var dataSourceArray:[[String:AnyObject]]?
    
    var delegate: SpinnerItemSelectedProtocol?
    @IBOutlet weak var spTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        self.view.isOpaque = false

        
        self.view.endEditing(true)
        
        let nib = UINib(nibName: "SpinnerTableCell", bundle: nil);
        self.spTableView.register(nib, forCellReuseIdentifier: reuseIdentifier)
        self.spTableView.delegate = self;
        self.spTableView.dataSource = self;
        self.spTableView.reloadData()
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataSourceArray!.count
    }

  
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SpinnerTableCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! SpinnerTableCell
        
        let item = self.dataSourceArray![indexPath.row]
         cell.spLabel.text = item[app.englishLocale() ? "name_en" : "name_ar"] as! String?
        // Configure the cell...

        return cell
    }

    
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        delegate!.itemSelected!(selectedItem: self.dataSourceArray![indexPath.row])
//        let item:MenuItem  = dataSourceArray[indexPath.row]
//        switch item.storyboardId! {
//        case cids.rootNavigationCtrl:
//            Cuts.present(vc: self, storyid: item.storyboardId!)
//            break;
//        case cids.aboutUsController
//        , cids.contactUsController
//        , cids.useConditionsViewController
//        , cids.registerController:
//            Cuts.push(vc: self, storyid: item.storyboardId!)
//            break;
//        default:
//            break
//            
//        }
        
        self.dismiss(animated: true, completion: nil)
        
    }

    
}
