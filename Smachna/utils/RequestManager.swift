//
//  RequestManager.swift
//  Super Market
//
//  Created by mac on 11/27/16.
//  Copyright © 2016 mac. All rights reserved.
//

import Foundation



class  RequestManager {
    static func post(actionName:String , params:String! ,respo: @escaping ([String: Any])->()){
    
        let url = constants.BASE_URL + actionName;
        
       print("\n \n ACTION = \(url)")
        print("METHOD POST")
        print("\n \n ACTION Parameters = \(params)")

        let geturl = URL(string: url)!
        var request = URLRequest(url: geturl)
        request.httpMethod = "POST"
       
        request.httpShouldHandleCookies = true
        if let prm = params{
        request.httpBody = prm.data(using: String.Encoding.utf8)
        }
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) -> Void in
            print("\n \n RESPONSE = \(data)")

            if data != nil {
                
                
                let json = try? JSONSerialization.jsonObject(with: data!, options: [])
                if let dictionary = json as? [String: Any] {
//                    let s = dictionary;
                respo(dictionary)
                }
                
            }
        })
        task.resume()

    
    }
    
    
    
    static func  uploadRequest(image:UIImage,userid:Int)
    {
        
        
        let url = NSURL(string:constants.BASE_URL + "home/edit_img")
        
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST"
        
        let boundary = generateBoundaryString()
        
        //define the multipart request type
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        
        
        let image_data = UIImagePNGRepresentation(image)
        
        
        if(image_data == nil)
        {
            return
        }
        
        
        let body = NSMutableData()
        
        let fname = "test.png"
        let mimetype = "image/png"
        
        //define the data post parameter
        
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"test\"\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append("hi\r\n".data(using: String.Encoding.utf8)!)
        
        body.append("user_id=\(userid)\"\r\n".data(using: String.Encoding.utf8)!)

        
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"file\"; filename=\"\(fname)\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(image_data!)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        
        
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        
        
        
        request.httpBody = body as Data
        
        
        
        let session = URLSession.shared
        
        
        let task = session.dataTask(with: request as URLRequest) {
            (
            data, response, error) in
            
            guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                print("error")
                return
            }
            
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
        }
        
        task.resume()
        
        
    }
    
    
    static func generateBoundaryString() -> String
    {
        return "Boundary-\(NSUUID().uuidString)"
    }

    
    
    
    static func get(actionName:String , params:String! ,respo: @escaping ([String: Any])->()){
        
        let url = constants.BASE_URL + actionName;
        
        print("\n \n ACTION = \(url)")
        print("METHOD GET")

        print("\n \n ACTION Parameters = \(params)")
        let geturl = URL(string: url)!
        var request = URLRequest(url: geturl)
        request.httpMethod = "GET"
        
        request.httpShouldHandleCookies = true
        request.httpBody = params!.data(using: String.Encoding.utf8)
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) -> Void in
            print("\n \n RESPONSE = \(data)")

            if data != nil {
                
                
                let json = try? JSONSerialization.jsonObject(with: data!, options: [])
                if let dictionary = json as? [String: Any] {
                    //                    let s = dictionary;
                    respo(dictionary)
                }
                
            }
        })
        task.resume()
        
        
    }

}
