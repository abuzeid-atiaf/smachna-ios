//
//  CachingSideMenuController.swift
//  Example
//
//  Created by Teodor Patras on 15/07/16.
//  Copyright © 2016 teodorpatras. All rights reserved.
//

import UIKit
import SideMenuController

class DefaultSideMenuController: CustomSideMenuController{// , SideMenuDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let vc:HomeViewController = Cuts.vc(storyid: cids.HomeViewController) as! HomeViewController
        //vc.sideMenu = self
        performSegue(withIdentifier: "cachingEmbedSide", sender: nil)
        embed(centerViewController: UINavigationController(rootViewController:
            vc),
              cacheIdentifier:cids.HomeViewController)
    }
    
}
