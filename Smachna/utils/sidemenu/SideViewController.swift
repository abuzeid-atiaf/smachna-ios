//
//  CachingSideViewController.swift
//  Example
//
//  Created by Teodor Patras on 15/07/16.
//  Copyright © 2016 teodorpatras. All rights reserved.
//

import UIKit
import SideMenuController

class SideViewController: UITableViewController, SideMenuControllerDelegate {
    
    
    let dataSource :[MenuItem] = [
        
        MenuItem(title: "Home".localized, iconame: "menu_home", stid: cids.HomeViewController),
        MenuItem(title: "Call Us".localized, iconame: "menu_call", stid: cids.CallUsViewController),
        MenuItem(title: "Cart".localized, iconame: "menu_cart", stid: cids.CartViewController),
        MenuItem(title: "Current Orders".localized, iconame: "menu_order", stid: cids.CurrentOrdersViewController),
        MenuItem(title: "Offers".localized, iconame: "menu_offer", stid: cids.offersViewController),
        MenuItem(title: "Search".localized, iconame: "menu_search", stid: cids.SearchViewController),
        MenuItem(title: "Use Policy".localized, iconame: "menu_btn_use", stid: cids.useConditionsViewController),
        MenuItem(title: "About App".localized, iconame: "menu_about", stid: cids.aboutUsController),
        MenuItem(title: "Share App".localized, iconame: "menu_sharing", stid: cids.ACTION_SHARE)
        
    ]
    
    
    
    let cellIdentifier = "MenuTableCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        self.tableView.register(UINib(nibName: "MenuTableCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        sideMenuController?.delegate = self
    }
    
    
    func itemClicked(sender: AnyObject) {
        sideMenuController?.toggle()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let customView:menuHeaderView = Bundle.main.loadNibNamed("menu_header_view", owner: self, options: nil)?.first as? UIView as! menuHeaderView? {
            
            
            
            if(app.isUserLoggedIn()){
                
                // app.getUserData()
                // let url =  URL(string: constants.IMAGE_URL +  "");
                //customView.userImageView.kf.setImage(with: url);
                
                if let img = app.getUserImage(){
                    customView.userImageView.kf.setImage(with: URL(string:constants.IMAGE_URL + img))
                }
                
                let name = "\(app.getUserData()!.first_name) \(app.getUserData()!.last_name)"
                customView.loginOutlet.setTitle(name , for: .normal)
                customView.registerOutlet.isHidden = true
                
            }
            
            
            
            
            
            customView.registerOutlet.addTarget(self, action: #selector(onRegister(sender:)), for: .touchUpInside)
            customView.loginOutlet.addTarget(self, action: #selector(login(sender:)), for: .touchUpInside)
            
            customView.settingOutlet.addTarget(self, action: #selector(setting(sender:)), for: .touchUpInside)
            
            
            
            
            
            
            
            
            return customView
        }
        return nil
        
    }
    
    
    
    func onRegister(sender:UIButton){
        
        let vc:SignupViewController = Cuts.vc(storyid:cids.registerController) as! SignupViewController
        
        vc.backHome = cids.rootNavigationCtrl
        
        sideMenuController?.embed(centerViewController: vc)
        
    }
    
    
    
    func login(sender:UIButton){
        
        if app.isUserLoggedIn() {
            
        }else{
            Cuts.present(vc: self, storyid: cids.loginController)
            
        }
    }
    
    
    
    
    func setting(sender:UIButton){
        
        
        Cuts.present(vc: self, storyid: cids.settingViewController)
        
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(200)
    }
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell:MenuTableCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! MenuTableCell
        cell.titleLabel.text = dataSource[indexPath.row].item_title
        cell.iconImage.image = UIImage(named: dataSource[indexPath.row].item_icon!)
        
        if  dataSource[indexPath.row].storyboardId == cids.CurrentOrdersViewController{
        
        cell.countLbl.text = app.getCurrentOrdersCount()
            cell.countLbl.isHidden = false
        }
        
        if  dataSource[indexPath.row].storyboardId == cids.offersViewController{
          cell.titleLabel.textColor = UIColor(colorLiteralRed: 223/255, green: 26/255, blue: 78/255, alpha: 1)
        
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView,
                            didSelectRowAt indexPath: IndexPath) {
        
        if dataSource[indexPath.row].storyboardId == cids.HomeViewController {
            Cuts.present(vc: self, storyid: cids.LaunchViewController)
        }else if dataSource[indexPath.row].storyboardId == cids.ACTION_SHARE {
            Utils.share(vc: self, objectsToShare: [app.getAppShareLink()])
            
        }else{
            
            if dataSource[indexPath.row].storyboardId == cids.CurrentOrdersViewController {
                if app.isUserLoggedIn() == false {
                    Cuts.present(vc: self, storyid: cids.loginController)
                    return;
                }
                
            }
            sideMenuController?.embed(centerViewController: Cuts.vc(storyid: dataSource[indexPath.row].storyboardId!))
            
        }
    }
    
    
    
    
    
    
    func sideMenuControllerDidHide(_ sideMenuController: SideMenuController) {
        
        
        
        
        print(#function)
    }
    
    func sideMenuControllerDidReveal(_ sideMenuController: SideMenuController) {
        print(#function)
    }
}
