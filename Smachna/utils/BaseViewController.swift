        
        
        //
        //  BaseViewController.swift
        //  Super Market
        //
        //  Created by mac on 11/19/16.
        //  Copyright © 2016 mac. All rights reserved.
        //
        
        import UIKit
        
        import Kingfisher
        import SideMenuController
        
        class BaseViewController: UIViewController  , UITabBarDelegate {
            var navBarView:navigation_bar_view?
            var backHome = ""
            ///START====================( STORYBOARD VIEWS )==============
            
            ///@END====================( STORYBOARD VIEWS )===============
            private let cart_tag = 9
            private let  profile_tag = 8
            
            private let  share_tag = 7
            private let  offers_tag = 6
            var cartX:Float = 0
            var cartY:Float = 0 ;
            var menuHidden = true;
            
            
            
            
            
            
            
            
            override func viewDidLoad() {
                super.viewDidLoad()
                let uuid = NSUUID().uuidString
                print("sixUUID===\(uuid)")
                
                
                
                //self.view.backgroundColor = UIColor.init(patternImage:UIImage(named: "act_bg")!)
                // setSideMenuControllers()
                // Do any additional setup after loading the view.
                
                
                //self.regis
                NotificationCenter.default.addObserver(self, selector: #selector(BaseViewController.showCartBadge), name:NSNotification.Name(rawValue: constants.UPDATE_CART_COUNT), object: nil)
                
                
                
                
            }
            
            func updateCartCount(){
                
                if let cart = app.getCartItems(){
                    if cart.count > 0{
                        navBarView?.cartCountLbl.text  = "\(cart.count)"
                    }else{
                        navBarView?.cartCountLbl.isHidden = true
                        
                        
                    }
                }
            }
            func showCartBadge(){
               // navBarView?.cartCountLbl.isHidden = false
                
                updateCartCount()
            }
            
            deinit {
                NotificationCenter.default.removeObserver(self)
            }
            
            
            
            // MARK: - Navigation
            
            // In a storyboard-based application, you will often want to do a little preparation before navigation
            override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
                // Get the new view controller using segue.destinationViewController.
                // Pass the selected object to the new view controller.
            }
            func toggleMenuAction(sender:Any?){
                //        mdelgate.item
                // self.embed(centerViewController: someUIViewControllerInstance)
                
            }
            
            
            
            
            func setNavgationBar(hasMenu:Bool , title:String?,subTitle:String? , showSearch:Bool  ,back:Bool ,cart:Bool )  {
                self.navigationController?.isNavigationBarHidden = true
                
                //        self.navigationController?.navigationController.hidenav
                navBarView = Bundle.main.loadNibNamed("navigation_bar", owner: self, options: nil)?.first as? UIView as! navigation_bar_view?
                
                var deviceFrame = navBarView!.frame
                var deviceBounds = navBarView!.bounds
                deviceBounds.size.width = self.view.bounds.size.width;
                deviceFrame.size.width = self.view.frame.size.width;
                navBarView!.cartCountLbl.layer.cornerRadius =  navBarView!.cartCountLbl.frame.width / 2
                
                navBarView!.cartCountLbl.clipsToBounds = true
                navBarView!.menuOutlet.isHidden  = !hasMenu
                if ( subTitle != nil) {
                    deviceBounds.size.height  = 104;
                    deviceFrame.size.height   =   104;
                    navBarView!.titleLayout.isHidden = false;
                    
                    navBarView!.navTitleLabel.text = subTitle;
                }else{
                    deviceFrame.size.height =  64;
                    deviceBounds.size.height = 64;
                    navBarView!.titleLayout.isHidden =  true;
                }
                
                
                
                
                
                
                navBarView!.frame = deviceFrame;
                navBarView!.bounds = deviceBounds;
                
                updateCartCount()
                
                navBarView!.backBtnOutlet.isHidden = !back
                navBarView!.searchIcon.isHidden = !showSearch
                navBarView!.cartOutlet.isHidden = !cart
                navBarView!.cartCountLbl.isHidden = !cart
                
                
                if(app.englishLocale()){
                    navBarView?.smallBackOutlet.image = UIImage(named:"arrow_left")
                    navBarView?.appLogo.image = #imageLiteral(resourceName: "ac_logo_en")
                }
                
                
                if title == nil {
                    navBarView!.navTitle.isHidden = true
                    navBarView!.appLogo.isHidden = false
                }else{
                    navBarView!.appLogo.isHidden = true
                    navBarView!.navTitle.isHidden = false
                    navBarView!.navTitle.text  = title
                    
                    
                    
                }
                
                
                
                if (app.englishLocale()){
                    
                    navBarView?.backBtnOutlet.setImage(UIImage(named:"ac_backـleft"), for: .normal)
                    
                }
                navBarView!.menuOutlet.addTarget(self, action: #selector(toggleMenu), for: .touchUpInside)
                navBarView!.searchIcon.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
                navBarView!.backBtnOutlet.addTarget(self, action: #selector(dismissButton), for: .touchUpInside)
                let gest = UITapGestureRecognizer(target: self, action: #selector(home(sender:)))
                navBarView?.smallBackOutlet.addGestureRecognizer(gest)
                
                navBarView!.cartOutlet.addTarget(self, action: #selector(cartButton), for: .touchUpInside)
                
                
                
                
                self.view.addSubview(navBarView!)
                
                
                
                
                
                
                
            }
            
            func addBottomTabBar(){
                
                //
                let bar :UITabBar = UITabBar(frame: CGRect(x: 0, y: self.view.frame.size.height-50, width: self.view.frame.size.width, height: 50))
                
                let item1 = UITabBarItem(title: "cart".localized, image: UIImage(named: "bottom_basket"), selectedImage: UIImage(named: "bottom_basket"))
                item1.tag = cart_tag
                let item2 = UITabBarItem(title: "offers".localized, image: UIImage(named: "bottom_notifcations"), selectedImage: UIImage(named: "bottom_notifcations"))
                item2.tag = offers_tag
                let item3 = UITabBarItem(title: "Profile".localized, image: UIImage(named: "bottom_profile"), selectedImage: UIImage(named: "bottom_profile"))
                item3.tag = profile_tag
                let item4 = UITabBarItem(title: "share".localized, image: UIImage(named: "bottom_share"), selectedImage: UIImage(named: "bottom_share"))
                
                item4.tag = share_tag
                
                bar.delegate = self
                bar.items = [item1,item2,item3,item4]
                bar.selectedItem  = item1
                //        let x = UIView(frame:deviceFrame!)
                
                
                
                bar.tintColor = UIColor.red
                self.view.addSubview(bar)
                
                
                if app.englishLocale() {
                    cartX = 40
                }else {
                    cartX = Float(self.view.frame.size.width)  -  Float(60)
                }
                cartY = Float(self.view.frame.size.height) - Float(bar.frame.height - 8 )
                
                //if (bar.viewWithTag(cart_tag)  != nil)
                //     cartY = Float((bar.viewWithTag(cart_tag)?.center.y)!)  + Float(bar.center.y)
                //    cartX = Float(bar.center.x ) + Float((bar.viewWithTag(cart_tag)?.center.x)!)
            }
            func cartButton( sender:UIButton){
                Cuts.present(vc: self, storyid: cids.CartViewController)
                
            }
            func home( sender:UIButton){
                Cuts.present(vc: self, storyid: cids.rootNavigationCtrl)
            }
            func dismissButton( sender:UIButton){
                
                
                if (backHome == cids.dismiss || backHome == "" ){
                    if(self.navigationController?.popViewController(animated: true) == nil){
                        self.dismiss(animated: true, completion: nil);
                    }
                }else{
                    
                    Cuts.present(vc: self, storyid: backHome)
                    
                }
                
                
                
                
                
            }
            
            
            func searchAction( sender:UIButton){
                
                let vc = Cuts.vc(storyid: cids.SearchViewController)
                if let menu = sideMenuController {
                    menu.embed(centerViewController: vc)
                }
                // Cuts.present(vc: self, storyid: cids.SearchViewController)
            }
            
            
            
            func toggleMenu( sender:UIButton){
                sideMenuController?.toggle()
            }
            
            func showVC( vc:BaseViewController){
                
                if let menu = sideMenuController {
                    menu.embed(centerViewController: vc)
                }
            }
            override func didReceiveMemoryWarning() {
                super.didReceiveMemoryWarning()
                // Dispose of any resources that can be recreated.
            }
            
            func tabBar(_ tabBar: UITabBar,
                        didSelect item: UITabBarItem)
                
            {
                
                
                
                switch item.tag {
                case cart_tag:
                    Cuts.push(vc: self, storyid: cids.CartViewController)
                    break
                case profile_tag:
                    Cuts.push(vc: self, storyid: cids.profileController)
                    break
                case offers_tag:
                    Cuts.push(vc: self, storyid: cids.offersViewController)
                    break
                case share_tag:
                    Utils.share(vc: self, objectsToShare: ["link here to share"])
                    break
                default:
                    print("not")
                    break
                }
                //Toast.alert(message: "didSelect item")
                
                
            }
            
            
            
            
        }
        
        
        extension BaseViewController {
            
            func hideKeypadWithTextFields(){
                let gest = UITapGestureRecognizer(target: self, action: #selector(hideKeypad))
                
                view.addGestureRecognizer(gest)
                
            }
            
            
            
            func hideKeypad(sender:UIView){
                view.endEditing(true)
                
                for v in view.subviews {
                    if v === UITextField.self {
                        v.endEditing(true)
                        
                    }
                }
                
                
            }
            
            
            
        }
