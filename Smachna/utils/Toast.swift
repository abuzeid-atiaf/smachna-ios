//
//  Toast.swift
//  Super Market
//
//  Created by Abuzeid on 11/28/16.
//  Copyright © 2016 mac. All rights reserved.
//

import UIKit
import Foundation
import ARSLineProgress
class Toast{
    
    
    static func showFail(){

    ARSLineProgress.showFail()
        
        
        
    }
    
    static func showSuccess(){
        
        ARSLineProgress.showSuccess()
        
        
        
    }
    
    

    static func alert(message:String){
        
        
     let alert = UIAlertController(title:nil , message: message, preferredStyle: UIAlertControllerStyle.alert)
        
       // alert.view.backgroundColor = UIColor.black
        
        if  UIApplication.shared.keyWindow?.rootViewController != nil{
        
        
             UIApplication.shared.keyWindow!.rootViewController!.present(alert, animated: true, completion: nil)
            
        }else{
        
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
        
        
        
        }
            
            
        
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2) ) {
            alert.dismiss(animated: true, completion: nil)
        }
        
    
    }

    static func alert(vc:UIViewController ,message:String){
        
        
        let alert = UIAlertController(title:nil , message: message, preferredStyle: UIAlertControllerStyle.alert)
        
       // alert.view.backgroundColor = UIColor.black
        
        vc.present(alert, animated: true, completion: nil)
        
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2) ) {
            alert.dismiss(animated: true, completion: nil)
        }
        
        
    }



}
