//
//  CartModel.swift
//  Super Market
//
//  Created by mac on 11/30/16.
//  Copyright © 2016 mac. All rights reserved.
//

import UIKit
import RealmSwift

class CartModel: Object {
    
    
    dynamic var name_ar = "";
    dynamic var name_en = "";
    dynamic var price = "";
    dynamic var quantity = "0";
    dynamic var id = "";
    dynamic var image = "";
    dynamic var extra = "";
    dynamic var extraid = "";

    override static func primaryKey() -> String? {
        return "id"
    }
}
