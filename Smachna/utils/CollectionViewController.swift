//
//  ViewController.swift
//  Super Market
//
//  Created by mac on 11/19/16.
//  Copyright © 2016 mac. All rights reserved.


import UIKit


import Kingfisher

private let reuseIdentifier = "CategoryCollectionCell"
private let sectionsCount = 2
/*
class HomeViewController: BaseViewController  ,UICollectionViewDelegate ,UICollectionViewDataSource  {
    
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var partCollection: UICollectionView!
    var dataSourceArray = [CategoryModel]();
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavgationBar(titleString: nil);
        let nib = UINib(nibName: "CategoryCollectionCell", bundle: nil);
        self.partCollection.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
        
        
        
        
        self.partCollection.delegate = self;
        self.partCollection.dataSource = self;
        let cellSize =  self.view.frame.size.width /  CGFloat(sectionsCount)
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0.0
        layout.minimumLineSpacing = 0.0
        layout.itemSize = CGSize(width:cellSize, height: cellSize)
        partCollection!.collectionViewLayout = layout

                self.getDataFromServer()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

//        let fr =  CGRect(x: 0, y:(loginView.frame.origin.y - loginView.frame.size.height ) , width: 0, height: 0)
//       loginView.frame = fr
//        loginView.bounds = fr
        
        
        loginView.isHidden = app.isUserLoggedIn()
        
    }
    func getDataFromServer(){
    
        Progress.show(sender: self);
        Alamofire.request(constants.BASE_URL + "home/category").responseJSON { response in
            let json = try? JSONSerialization.jsonObject(with: response.data!, options: [])
            if let dictionary = json as? [String: Any] {
                if let array = dictionary["category"] as? [Any] {
                    
                    for object in array {
                        
                        if let val = object as? [String: Any] {
                            var model = CategoryModel()
                            if let name = val["name_en"] as? String {
                                model.name_en = name;
                            }
                            
                            if let namear = val["name_ar"] as? String {
                                model.name_ar = namear;
                            }
                            
                            if let id = val["id"] as? String {
                                model.id = id;
                            }
                            
                            if let img = val["img"] as? String {
                                model.img = img;
                            }
                            
                            
                            self.dataSourceArray.append(model);
                        }
                        
                        
                        
                        
                        
                        
                    }//end for
                    DispatchQueue.main.async {
                        self.partCollection.reloadData();
                          Progress.hide()
                    }
                    
                }
                
            }
            
            
            
        }
        
        
    
    }
        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //
    //            func collectionView(collectionView: UICollectionView,
    //                                layout collectionViewLayout: UICollectionViewLayout,
    //                                sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
    //                return CGSize(width: 100 , height: 100)
    //            }
    ////    Use for interspacing
    //            func collectionView(collectionView: UICollectionView,
    //                                layout collectionViewLayout: UICollectionViewLayout,
    //                                minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
    //                return 1.0
    //            }
    //
    //            func collectionView(collectionView: UICollectionView, layout
    //                collectionViewLayout: UICollectionViewLayout,
    //                                minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
    //                return 1.0
    //            }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return  Utils.numberOfSectionsInCollectionView(array: self.dataSourceArray, sectionsCount: sectionsCount)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Utils.numberOfItemsInSection(section: section, array: self.dataSourceArray, sectionsCount: sectionsCount)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:CategoryCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CategoryCollectionCell
        let postion  = Utils.itemPostionInArray(indexPath: indexPath, sectionsCount: sectionsCount)
        let item  = self.dataSourceArray[postion]
        
         let url =  URL(string: constants.IMAGE_URL +  item.img!);
        cell.shopImage.kf.setImage(with: url)

        
        cell.shopNameLabel.text = app.englishLocale() ? item.name_en: item.name_ar;
        
        return cell
    }
    
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //getDataFromServer()
        let controller = self.storyboard?.instantiateViewController(withIdentifier: cids.shopSectionController) as! ShopSectionsViewController
        controller.productId = dataSourceArray[indexPath.row].id;
        
        controller.productName = app.englishLocale() ? dataSourceArray[indexPath.row].name_en : dataSourceArray[indexPath.row].name_ar  ;
        self.navigationController?.pushViewController(controller, animated: true);
        
        
    }
    
    
   }


*/

