//
//  app.swift
//  Super Market
//
//  Created by mac on 11/23/16.
//  Copyright © 2016 mac. All rights reserved.
//

import Foundation
import RealmSwift

class app{
    static var ordersCount = "currentorder";

    static var local = "local";
    static var exported = "exported";
    static var productLocaleExported = true
    
    static func englishLocale()->Bool{
         return LanguageManager.sharedInstance.getSelectedLocale() == constants.englishCode        
//        return UserDefaults.standard.value(forKey: constants.USER_PREF_LANG) as? String == constants.englishCode;
    }
    static func rememberUser(remember:Bool){
        
        UserDefaults.standard.set(remember, forKey: constants.REMEMBER_USER)
        UserDefaults.standard.synchronize()
        
    }
    static func setCurrentOrdersCount(count:String){
    UserDefaults.standard.set(count, forKey: ordersCount)
        
        UserDefaults.standard.synchronize()
        
    }
    
    static func getCurrentOrdersCount()->String{
        
     return    UserDefaults.standard.string(forKey:  ordersCount) ?? "0"
        
    }
    static func orderStatus(st:String)->String{
        
        switch st {
        case "0":
            return "waiting".localized
            
            
        case "1":
            return "processing now".localized
            
            
        case "2":
            return "received".localized
            
        default:
            return ""
        }
    }
    
    static func saveUserImage(img:String){
        UserDefaults.standard.set(img, forKey: "userimage");
        UserDefaults.standard.synchronize()
    
    }
    
    static func  getUserImage()->String?{
        return UserDefaults.standard.value(forKey: "userimage") as! String?;
        
        
    }
    
    
    
    static func getLocale()->String{
        let lang = UserDefaults.standard.value(forKey: constants.USER_PREF_LANG)
        if ( lang != nil) {
            return lang as! String;
            
        }
        return constants.defaultCode
    }
    static func saveAppLang(code:String){
        UserDefaults.standard.set(code, forKey: constants.USER_PREF_LANG)
        UserDefaults.standard.synchronize()
        
        
    }
    
    static func getAppShareLink()->String{
        
        return ""
    }
    static func removeUserData(){
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
        
    }
    
//    static func addOrUpdateItem(item:CartModel){
//        let realm = try! Realm()
//        
//        
//        try! realm.write {
//            
//            realm.add(item, update: true)
//        }
//        
//    }
    static func addItemToCart(item:CartModel,quantity:Float){
        
        let realm = try! Realm()
        var itemAdded  = false;
        //update or create new
        let result =  realm.objects(CartModel.self)
        for obj in result {
            if obj.id == item.id {
                try! realm.write {
                    let qnt = quantity +  Float(obj.quantity)!;
                    obj.quantity = String(qnt)
                    itemAdded = true
                    
                }
            }
            
            
        }
        
        
        
        if itemAdded  == false {
            try! realm.write {
                realm.add(item)
            }
        }
        
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: constants.UPDATE_CART_COUNT), object: nil)  
        
        
    }
    
    static func getCartItems()->Results<CartModel>?{
        let realm = try! Realm()
        return  realm.objects(CartModel.self)
        
        
    }
    
    static func removeCartItems(){
        let realm = try! Realm()
        let res = realm.objects(CartModel.self)
        
        try! realm.write {
            realm.delete(res)
        }
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: constants.UPDATE_CART_COUNT), object: nil)
    }
    
    static func removeCartItems(item:CartModel){
        let realm = try! Realm()
        
        
        try! realm.write {
            realm.delete(item)
        }
        
 NotificationCenter.default.post(name: NSNotification.Name(rawValue: constants.UPDATE_CART_COUNT), object: nil)    }
    
    
    static func saveUserData(user:UserModel){
        let realm = try! Realm()
        try! realm.write {
            let res = realm.objects(UserModel.self)
            realm.delete(res)
            realm.add(user)
        }
    }
    
    static func getUserData()->UserModel?{
        let realm = try! Realm()
        let res =  realm.objects(UserModel.self)
        
        if(res.count > 0){
            
            return res.first!;
        }
        return nil
        
    }
    
    static func isUserLoggedIn()->Bool{
        let realm = try! Realm()
        let res =  realm.objects(UserModel.self)
        
        if(res.count > 0){
            
            return true
        }
        return false
        
    }
    static func changeLnagToEn(){
    
        
       
            L102Language.setAppleLAnguageTo(lang: constants.englishCode)
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
        
    }
    static func changeLangToAr(){
       
            L102Language.setAppleLAnguageTo(lang: constants.arabicCode)
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        
        
        
        
        
    }
    static func setRoot(storyid:String){
        let transition: UIViewAnimationOptions = .transitionCurlUp

        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController = Cuts.mainstory().instantiateViewController(withIdentifier:storyid)
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0.55001, options: transition, animations: { () -> Void in
        }) { (finished) -> Void in
            
        }
        

    }
    
    static var localeType:String?
    
    static  func  userFromDic(data:[String:Any]) ->UserModel?{
        let user:UserModel = UserModel()
        
        
        user.username = data["username"]! as! String
        user.first_name = data["first_name"]! as! String
        user.mobile = data["mobile"]! as! String
        user.home = data["home"]! as! String
        user.region_id = data["region_id"]! as! String
        user.jada = data["jada"]! as! String
        user.city_id = data["city_id"]! as! String
        user.street = data["street"]! as! String
        user.email = data["email"]! as! String
        user.userid = data["id"]! as! String
        return user;
        
        
    }
    
}
