//
//  ProfileViewController.swift
//  Super Market
//
//  Created by mac on 11/28/16.
//  Copyright © 2016 mac. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController,UITableViewDelegate ,UITableViewDataSource {

    @IBOutlet weak var profTable: UITableView!
    
    private let reuseIdentifier  = "cell_profile"
    private let id_share = "share"
    private let id_edit = "1"
    private let id_cart = "2"
    private let id_logout = "3"
    
    var userData: UserModel?
    
    var dataSourceArray=[MenuItem]();
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.setNavgationBar(titleString: "Profile".localized);
         self.setNavgationBar(hasMenu: true,title: nil, subTitle: nil, showSearch: true, back: false, cart: false)
        self.addBottomTabBar()
              userData  = app.getUserData()
        
       
        
        if userData != nil {
        
            
            
            dataSourceArray.append(MenuItem(title: userData!.username ,iconame: "",stid: ""));
            
            
            dataSourceArray.append(MenuItem(title: "mobile".localized  + " "  + userData!.mobile ,iconame: "pro_mobile",stid: ""));
            dataSourceArray.append(MenuItem(title: "cart".localized,iconame: "pro_basket",stid: id_cart));
            dataSourceArray.append(MenuItem(title: "edit".localized,iconame: "pro_edit",stid: id_edit));
            dataSourceArray.append(MenuItem(title: "share app".localized,iconame: "pro_share",stid: id_share));
            dataSourceArray.append(MenuItem(title: "log out".localized,iconame: "pro_log_out",stid: id_logout));
            
            

        }
        
        let nib = UINib(nibName: "ProfileTableCell", bundle: nil);
        self.profTable.register(nib, forCellReuseIdentifier: reuseIdentifier)
        self.profTable.delegate = self;
        self.profTable.dataSource = self;
        self.profTable.reloadData()
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataSourceArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
       
        /*
        if indexPath.row == 2 {
            cell.badgeLabel.isHidden = false
            cell.badgeLabel.text = String(app.getCartItems().count)
        }else{
        cell.badgeLabel.isHidden = true
        }
        cell.titleLabel.text = dataSourceArray[indexPath.row].item_title
        cell.icoImageView.image = UIImage(named:dataSourceArray[indexPath.row].item_icon!);
        
        if indexPath.row == 0 {
        cell.icoImageView.isHidden = true
        cell.titleLabel.textAlignment = .center
            
            cell.titleLabel.textColor = UIColor.blue
        }
        
        
        if( indexPath.row  == self.dataSourceArray.count - 1 ) {
            cell.titleLabel.textColor = UIColor.red
        }
*/
        // Configure the cell...
        
        return cell
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let item:MenuItem  = dataSourceArray[indexPath.row]
     
        switch item.storyboardId! {
        case id_share:
            Utils.share(vc: self, objectsToShare:[app.getAppShareLink()])
            break;
        case id_edit:
            //goto signup an
            break;
            
        case id_cart:
            Cuts.push(vc: self, storyid: cids.CartViewController)
            break;
            
            
        case id_logout:
            app.removeUserData()
            Toast.alert(vc:self ,message: "you recently logged out".localized)
            Cuts.present(vc: self, storyid: cids.rootNavigationCtrl)
            
            break;
            
            
        default:
            break
            
        }
        
        self.dismiss(animated: true, completion: nil)
        
    }


}
