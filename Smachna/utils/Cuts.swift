//
//  Cuts.swift
//  Super Market
//
//  Created by mac on 11/28/16.
//  Copyright © 2016 mac. All rights reserved.
//

import Foundation
import UIKit
class Cuts {
    
    static func mainstory()->UIStoryboard {
        
        return UIStoryboard(name: "Main", bundle: nil);
    }
    static func push(vc:UIViewController,storyid:String){
        let controller =  UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: storyid)
        
        vc.navigationController?.pushViewController(controller, animated: true);
        
    }
    
    
    static func present(vc:UIViewController,storyid:String){
        let controller =  UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: storyid)
        
        vc.present(controller, animated: true);
        
    }
    
    
    
    
    
    static func vc(storyid:String)->UIViewController{
        return  UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: storyid)
        
    }
}
