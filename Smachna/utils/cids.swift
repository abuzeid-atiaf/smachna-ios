//
//  File.swift
//  Super Market
//
//  Created by mac on 11/22/16.
//  Copyright © 2016 mac. All rights reserved.
//

import Foundation

struct cids{
    static let ACTION_SHARE = "shareapp"
static let dismiss = "dismiss";
    static let doubleDismiss = "doubleDismiss";

    
    
    static let HomeViewController = "HomeViewController"
    static let alertContinueViewController = "AlertContinueViewController"
    
    static let LaunchViewController = "LaunchViewController"


    static let forgetPasswordViewController = "ForgetPasswordViewController"

    static let PaymentMethodViewController = "PaymentMethodViewController"
    static let CallUsViewController = "CallUsViewController"
    static let ProductDetailsViewController = "ProductDetailsViewController"
       static let continueSignupViewController = "ContinueSignupViewController"
static let CartViewController = "CartViewController"
    static let CurrentOrdersViewController = "CurrentOrdersViewController"
    static let registerController = "SignupViewController"
    static let profileController = "ProfileViewController"
    static let loginController = "LoginViewController"
    static let rootNavigationCtrl = "DefaultSideMenuController"
    static let SearchViewController = "SearchViewController"
    static let aboutUsController = "AboutUsViewController"
    static let contactUsController = "CallUsViewController"
    static let spinnerViewController = "SpinnerViewController"
    static let offersViewController = "OffersViewController"
    static let useConditionsViewController = "UsePolicyViewController"
    
    
    static let settingViewController = "SettingViewController"
    static let ProLocaleViewController = "ProLocaleViewController"
    static let EditProfileViewController = "EditProfileViewController"
    static let ChangePassViewController = "ChangePassViewController"

}

