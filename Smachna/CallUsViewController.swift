//
//  CallUsViewController.swift
//  Smachna
//
//  Created by Mac on 12/8/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class CallUsViewController: BaseViewController ,UITableViewDataSource ,UITableViewDelegate{
    @IBOutlet weak var tableView: UITableView!
    
    var dataSource = [String]()
    var dataSourceIcons = [String]()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavgationBar(hasMenu: false,title: "Call Us".localized, subTitle: nil, showSearch: false, back: true, cart: false)
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.getDataFromServer()
        self.backHome = cids.rootNavigationCtrl
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    
    
    
    
    func getDataFromServer(){
        
        
        Progress.show(sender: self.view)
        
        RequestManager.post(actionName: "home/about", params: "", respo: {(dictionary) -> Void in
            Progress.hide()
            
            
            if let about = dictionary["about"] as?   [String:AnyObject] {
                
                self.dataSource.append(about["mobile"]! as! String)
                self.dataSource.append(about["mobile2"]! as! String)
//                self.dataSource.append(about["mobile3"]! as! String)
                self.dataSource.append(about["email"]! as! String)
                self.dataSource.append(about["address"]! as! String)
                self.dataSourceIcons.append("call")
                self.dataSourceIcons.append("call")
                self.dataSourceIcons.append("at")
                self.dataSourceIcons.append("map")
                self.dataSourceIcons.append("map")

                
            }
            DispatchQueue.main.async {
                
                self.tableView.reloadData()
            }
            
            
            
        })
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0,1:
            return 44;
        case 2,3:
            return 80;
        default:
            return 0
        }
    }
    func itemClicked(sender: AnyObject) {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "callus")!
        let textLabel: UILabel = cell.viewWithTag(4) as! UILabel
        
        textLabel.text = self.dataSource[indexPath.row]
        let iconView:UIImageView = cell.viewWithTag(3) as! UIImageView
        
        
                iconView.image = UIImage(named: dataSourceIcons[indexPath.row])
        
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        
         let text = self.dataSource[indexPath.row]
        
        if indexPath.row == 0 || indexPath.row == 1 {
            if let url = URL(string: "tel://\(text)") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)//(url)
            }else{
            //text.replacingOccurrences(of: "-", with: "")
            }
      
        }else{
        //
        
        }
        
        
    }
    
}
