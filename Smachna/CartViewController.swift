//
//  CartViewController.swift
//  Smachna
//
//  Created by Mac on 12/8/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class CartViewController: BaseViewController, UIPopoverPresentationControllerDelegate, UITableViewDelegate , UITableViewDataSource ,GeneralCallbackDelegate{
    
    
    
    ///START====================( STORYBOARD VIEWS )==============
    @IBOutlet weak var tableView: UITableView!
    
    
    
    @IBOutlet weak var totalLbl: UILabel!
    
    func callbackObject( obj:AnyObject){
        
        
        //paymentMethod = obj as! String;
    }
    
    
    
    @IBAction func orderAction(_ sender: Any) {
        
        
        
        if(dataSource.count<1){
            Toast.alert(vc: self, message: "Cart is empty please add items first".localized)
            return;
        }
        
        var prameters = "total_price=" +  getTotalPrice()
        prameters +=  "&products=\(Utils.orderJsonFromArray(dataSource: dataSource))"
        
        if app.isUserLoggedIn(){
            
            
            let user = app.getUserData()
            prameters += "&user_id=" + user!.userid
            prameters += "&client_name=" + user!.username
            prameters += "&client_mobile=" + user!.mobile
            prameters += "&region_id=" + user!.region_id
            prameters += "&city_id=" + user!.city_id
            prameters += "&street=s " + user!.street
            prameters += "&jada=" + user!.jada
            prameters += "&home=" + user!.home
            //gototpay
            
            
            let vc:PaymentMethodViewController = Cuts.vc(storyid: cids.PaymentMethodViewController) as! PaymentMethodViewController
            
            vc.prams = prameters
            vc.modalPresentationStyle = .overCurrentContext //presentation style
            present(vc, animated: true, completion: nil)
            vc.popoverPresentationController?.sourceView = view
            
            let mv :UIView = sender as! UIView
            vc.popoverPresentationController?.sourceRect = mv.frame
            
            
        }else{
            
            
            let cont:ContinueSignupViewController = Cuts.vc(storyid: cids.continueSignupViewController) as! ContinueSignupViewController
            
            
            cont.prameters = prameters
            cont.parentControllerID = cids.dismiss
            cont.controllerTask = constants.flag_task_order
            self.present(cont, animated: true, completion: nil)
        }
        
        
    }
    
    ///@END====================( STORYBOARD VIEWS )===============
    
    
    func adaptivePresentationStyleForPresentationController(PC: UIPresentationController!) -> UIModalPresentationStyle {
        // This *forces* a popover to be displayed on the iPhone
        return .none
    }
    
    
    
    
    var dataSource = [CartModel]()
    let cellIdentifier = "CartTableCell"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backHome = cids.rootNavigationCtrl
        self.setNavgationBar(hasMenu: true,title: nil, subTitle: nil, showSearch: false, back: true, cart: false)
        // self.navBarView?.backBtnOutlet.addTarget(self, action: #selector(home1(sender:)), for: .touchUpInside)
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.reloadData()
        self.tableView.register(UINib(nibName: "CartTableCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        if let cart = app.getCartItems(){
            for item in cart{
                self.dataSource.append(item)
            }
        }
        updateTotalPrice()
        
    }
    
    func updateTotalPrice(){
        
        self.totalLbl.text = getTotalPrice() + "  " + "Dinar".localized
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    func removeItem(sender: AnyObject) {
        let item: CartModel = self.dataSource[sender.tag]
        
        self.dataSource.remove(at: self.dataSource.index(of: item)!)
        
        self.tableView.reloadData()
        app.removeCartItems(item: item)
        //
        //        for item in app.getCartItems(){
        //            self.dataSource.append(item)
        //        }
        //
        updateTotalPrice()
    }
    func getTotalPrice()->String{
        var totalPrice:Float = 0.0
        
        
        for item in dataSource {
            let newprice = Float(item.price)! * Float(item.quantity)!
            totalPrice += (newprice);
        }
        
        return String(totalPrice)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.backHome = cids.rootNavigationCtrl
        
    }
    func plus(sender: AnyObject) {
        let item: CartModel = self.dataSource[sender.tag]
         app.addItemToCart(item: item , quantity: 0.5)
        updateTotalPrice()
        
    }
    
    
    func minus(sender: AnyObject) {
        let item: CartModel = self.dataSource[sender.tag]
        
        if(Float(item.quantity)!>0){
            app.addItemToCart(item: item ,quantity: -0.5)
            
        }else{
            self.dataSource.remove(at: sender.tag)
            
            self.tableView.reloadData()
            app.removeCartItems(item: item)
            
        }
        updateTotalPrice()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:CartTableCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! CartTableCell
        
        
        let item: CartModel = self.dataSource[indexPath.row]
        let name = app.englishLocale() ?item.name_en:item.name_ar
        cell.ctitleLbl.text = name
        cell.cpriceLbl.text = item.price
        cell.qntLbl.text = item.quantity
        
        cell.quantity = Float(item.quantity)!
        let url =  URL(string: constants.IMAGE_URL +  item.image);
        
        
        cell.extraTitle.text = item.extra
        
        cell.civ.kf.setImage(with: url)
        cell.removeOutlet.tag = indexPath.row
        cell.removeOutlet.addTarget(self, action: #selector(removeItem(sender:)), for: .touchUpInside)
        
        cell.plusOutlet.tag = indexPath.row
        cell.plusOutlet.addTarget(self, action: #selector(plus(sender:)), for: .touchUpInside)
        
        cell.minusOutlet.tag = indexPath.row
        cell.minusOutlet.addTarget(self, action: #selector(minus(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    
    
    
}
