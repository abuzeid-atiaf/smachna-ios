//
//  ChangePassViewController.swift
//  Smachna
//
//  Created by Mac on 1/15/17.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class ChangePassViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavgationBar(hasMenu:false ,title: "change password".localized, subTitle: nil, showSearch: false, back: true, cart: false)

self.hideKeypadWithTextFields()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBOutlet weak var oldField: UITextField!
    @IBOutlet weak var newField: UITextField!
    @IBOutlet weak var confirmField: UITextField!
    @IBAction func changePass(_ sender: Any) {
        
        
        
        if  Utils.textFieldsNotEmpty(tf: self.oldField,self.newField,confirmField) {
           // self.login()
            
        }else{
            
            Toast.alert(vc:self ,message: "please enter all fields".localized)
            
            
        }

    }
    
    
    
    
    
    func login(){
        
       // let token = FIRInstanceID.instanceID().token()!
        
        
        var prm = "username="
            
            
//            + self..text! ;
//        
//        prm += "&password=" + self.passField.text!
//        prm += "&devicetoken=" + token
//        
//        prm += "&devicetype=" + constants.DEVICE_TYPE
//        
        
        Progress.show(sender: self.view)
        
        RequestManager.post(actionName: "home/login", params: prm, respo: {(dictionary) -> Void in
            Progress.hide()
            
            if let userLogged = dictionary["success"]  {
                if ((userLogged as! String ) == "true"  ){
                    let data =  dictionary["user"] as!  [String:String]
                    
                    app.saveUserData(user: app.userFromDic(data: data)!)
                    DispatchQueue.main.async{
                        Cuts.present(vc: self, storyid: cids.rootNavigationCtrl)
                    }
                }else{
                    DispatchQueue.main.async{
                        Toast.alert(vc: self,message: "check your user name or mobile".localized)
                    }
                    
                }
                
                
                
                
            }
            
            
            if dictionary["validation"] != nil  {
                
                Toast.alert(vc:self ,message: "your username or password incorrect".localized)
                
            }
            
            
            
        })
    }
    
    
}
