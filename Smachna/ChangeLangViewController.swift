//
//  ChangeLangViewController.swift
//  Smachna
//
//  Created by Mac on 1/11/17.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class ChangeLangViewController: BaseViewController {

    @IBOutlet weak var conainerview: UIView!
    @IBOutlet weak var enButton: DLRadioButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.clear
        self.view.isOpaque = false
    
    }

    @IBAction func artap(_ sender: Any) {
        ar()
    }
    @IBAction func artouch(_ sender: DLRadioButton) {
        
        if sender.isSelected{
            
           ar()
        
        
        }
        
    }
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func ar(){
    
    
        app.changeLangToAr()
        
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1) ) {
            
            app.setRoot(storyid:cids.ProLocaleViewController)
        }
        
        
    }
    func en(){
        
        
        //Toast.alert(vc: self,message: "its prefered to restart the app to reset locale".localized)
        LanguageManager.sharedInstance.setLocale(constants.englishCode)
        app.changeLnagToEn()
       
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1) ) {
            
            app.setRoot(storyid:cids.ProLocaleViewController)
        }
        
        
    }
    @IBAction func engtap(_ sender: Any) {
        
        en()
    }
    @IBAction func touchinside(_ sender: DLRadioButton) {
        
        if sender.isSelected{
            
           en()
        }
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
        
        
    }
    


}
