//
//  EditProfileViewController.swift
//  Smachna
//
//  Created by Mac on 1/12/17.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import Alamofire


class EditProfileViewController: BaseViewController ,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var passfield: UITextField!
    
    @IBOutlet weak var userIV: UIImageView!
    let userImagePicker = UIImagePickerController()
    
    var selectedimage:UIImage?
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        self.selectedimage = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.userIV.image = self.selectedimage
        
        
               dismiss(animated: true, completion: nil)
         let prams = "\(app.getUserData()!.userid)".data(using: String.Encoding.utf8)!
        
        
        
        
        
        Progress.show(sender: self)
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                
                
                if let _image = self.selectedimage {
                    if let imageData = UIImageJPEGRepresentation(_image, 0.5) {
                        multipartFormData.append(imageData, withName: "file", fileName:  "file.jpg", mimeType: "image/jpg")
                    }
                }
                
                
                
                
                
                multipartFormData.append(prams, withName: "user_id")
        },
            to: constants.BASE_URL +  "home/edit_img" ,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                    
                    
                    
                case .success(let upload, _, _):
                    
                    
                    upload.responseJSON {
                        response in
                        
                        
                        debugPrint(response)
                        
                        Progress.hide()
                        if let responsedic = response.result.value as! [String:String]? {
                            if let img  = responsedic["img"]  {
                                app.saveUserImage(img:img)
                            }
                            
                            
                        }
                        
                        
                    }
                case .failure(let encodingError):
                    Progress.hide()
                    print(encodingError)
                }
        }
        )
        
        
        
        
        
        
        
    }
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavgationBar(hasMenu: false,title: "Edit Profile".localized, subTitle: nil, showSearch: false, back: true, cart: false)
        self.backHome = cids.dismiss
        self.userImagePicker.delegate = self
        
        let user = app.getUserData();
        usernameField.text = user!.username
        phoneField.text = user!.mobile
        emailField.text = user!.email
        passfield.text  = user!.password
        if let img = app.getUserImage(){
            self.userIV.kf.setImage(with: URL(string:constants.IMAGE_URL + img))}
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    
    
    
    
    func sendRegisterDataToServer(){
        
        self.view.endEditing(true)
        
        let user = app.getUserData();
        var prameters = "user_name=" + usernameField.text!
        prameters += "&user_id=\(user!.userid)"
        prameters += "&phone=" + phoneField.text!
        prameters += "&email=" + emailField.text!
        prameters += "&home=\(user!.home)"
        prameters += "&jada=\(user!.jada)"
        prameters += "&region_id=\(user!.region_id)"
        prameters += "&city_id=\(user!.city_id)"
        prameters += "&street=\(user!.street)"
        
        
        
        prameters += "&passwrod=\(passfield.text!)"

        
        
        
        Progress.show(sender: self);
        
        
        
        
        Progress.show(sender: self);
        RequestManager.post(actionName: "home/edit_address", params: prameters , respo: {(dictionary) -> Void in
            Progress.hide()
            if let userLogged = dictionary["success"]  {
                
                
                if ((userLogged as! String ) == "true"  ){
                    if let userdic = dictionary["user"] as! [String : String]?{
                        app.saveUserData(user: app.userFromDic(data: userdic)!)
                        
                    }
                   /// self.view.isUserInteractionEnabled = false
                     DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3) ) {
                       // self.dismiss(animated: true, completion: nil)
                      Cuts.present(vc: self, storyid: cids.rootNavigationCtrl)
                    }
                    
                    Toast.alert(vc: self, message: "Your profile updated".localized)

                    
                }else{
                    if let er =  dictionary["validation"]  as! [String:String]? {
                        
                        DispatchQueue.main.async{
                            if( er.first?.key == "mobile"){
                                
                                Toast.alert(vc: self, message: "the mobile number already registered".localized)
                            }else if (er.first?.key == "username"){
                                Toast.alert(vc: self, message: "the username already registered".localized)
                                
                            }else if (er.first?.key == "email"){
                                Toast.alert(vc: self, message: "the email already registered".localized)
                                
                                
                            }else{
                                
                                Toast.alert(vc: self, message: (er.first?.value)!)
                                
                            }
                        }
                        
                    }
                }
                
                
                
            }
        })
        
    }
    
    
    
    @IBAction func chooseimage(_ sender: Any) {
        
        
        
    }
    @IBAction func img(_ sender: Any) {
        userImagePicker.sourceType = .photoLibrary
        
        present(userImagePicker, animated: true, completion: nil)
    }
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet var baseScrollView: UIScrollView!
    
    @IBAction func changePassword(_ sender: Any) {
        let change = UIStoryboard(name: "UtilsStoryboard", bundle: nil).instantiateViewController(withIdentifier: cids.ChangePassViewController)
        
        self.present(change, animated: true, completion: nil)
        
        
        
        
    }
    @IBAction func saveData(_ sender: Any) {
        self.view.endEditing(true)
        
        
        if Utils.textFieldsNotEmpty(tf: self.usernameField,self.emailField,passfield,phoneField){
            
            sendRegisterDataToServer()
            
        }else{
            
            Toast.alert(vc:self ,message: "check your inputs".localized)
        }
    }
    @IBAction func changeImageAction(_ sender: Any) {
        
        
    }
    @IBOutlet weak var saveAction: UIButton!
    
    
    
    
    
    
}
