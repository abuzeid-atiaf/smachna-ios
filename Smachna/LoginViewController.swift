//
//  LoginViewController.swift
//  Smachna
//
//  Created by Mac on 12/8/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit


import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import BFPaperCheckbox
class LoginViewController: BaseViewController {
    
    ///START====================( STORYBOARD VIEWS )==============
    
    @IBOutlet weak var rememberCheckbox: BFPaperCheckbox!
    @IBOutlet weak var userField: UITextField!
    @IBOutlet weak var passField: UITextField!
    @IBAction func forgetPass(_ sender: Any) {
        
        Cuts.present(vc: self, storyid: cids.forgetPasswordViewController)
    }
    @IBAction func loginAction(_ sender: UIButton) {
    view.endEditing(true)
    
        if  Utils.textFieldsNotEmpty(tf: self.userField,self.passField) {
            self.login()

        }else{

            Toast.alert(vc:self ,message: "please enter your username and password".localized)

        
        }
    }
    @IBAction func registerAction(_ sender: Any) {
        
        Cuts.present(vc: self, storyid: cids.registerController)
    }
    ///@END====================( STORYBOARD VIEWS )===============
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavgationBar(hasMenu:false ,title: "Login".localized, subTitle: nil, showSearch: false, back: true, cart: false)
      // hideKeypadWithTextFields()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    
    
    
    
    
    

    
    func login(){
    
        let token = FIRInstanceID.instanceID().token()!

        
        var prm = "username=\(self.userField.text!)" ;
        
        prm += "&password=\(self.passField.text!)"
        prm += "&devicetoken=" + token
        
            prm += "&devicetype=\(constants.DEVICE_TYPE)"

        let isChecked = self.rememberCheckbox.isChecked
        Progress.show(sender: self.view)
        
        RequestManager.post(actionName: "home/login", params: prm, respo: {(dictionary) -> Void in
            Progress.hide()

            if let userLogged = dictionary["success"]  {
                if ((userLogged as! String ) == "true"  ){
                    if let data =  dictionary["user"] {
                    
                        app.rememberUser(remember: isChecked)
                        
                        app.saveUserData(user: app.userFromDic(data: data as! [String : Any])!)
                    }
                    
                    DispatchQueue.main.async{
                        Cuts.present(vc: self, storyid: cids.rootNavigationCtrl)
                    }
                }else{
                    DispatchQueue.main.async{
                        Toast.alert(vc: self,message: "check your user name or mobile".localized)
                    }
                    
                }
                
                
                
            
            }
            
            
            if dictionary["validation"] != nil  {

                Toast.alert(vc:self ,message: "your username or password incorrect".localized)
                
            }
            
            
            
        })
    }
    
    
    
    
}
