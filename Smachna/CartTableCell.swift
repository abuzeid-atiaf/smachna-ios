//
//  CartTableCell.swift
//  Smachna
//
//  Created by Mac on 12/6/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class CartTableCell: UITableViewCell {

    
    var quantity:Float  = 0.0 {
    
        didSet{
        
        self.qntLbl.text = String(quantity)
        
        }
    }
    @IBAction func plusAction(_ sender: Any) {
        quantity += 0.5
        
    }
    
    
    @IBAction func minusAction(_ sender: Any) {
        if quantity > 0 {
            quantity -= 0.5

        }

    }
    @IBOutlet weak var extraTitle: UILabel!
  
    @IBOutlet weak var ctitleLbl: UILabel!
    @IBOutlet weak var civ: UIImageView!
    
    @IBOutlet weak var cpriceLbl: UILabel!
    
    @IBOutlet weak var plusOutlet: UIButton!
    
    @IBOutlet weak var minusOutlet: UIButton!
    
    @IBOutlet weak var removeOutlet: UIButton!
    @IBOutlet weak var qntLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
