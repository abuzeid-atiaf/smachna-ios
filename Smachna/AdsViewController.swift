//
//  AdsViewController.swift
//  Smachna
//
//  Created by Mac on 12/8/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class AdsViewController: UIViewController {

    
    @IBOutlet weak var imageOutlet: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let image = UserDefaults.standard.value(forKey: "ads_image") as? String{
        
        let url = URL(string: constants.IMAGE_URL +  image)
            self.imageOutlet.kf.setImage(with: url)
        
        
        }

        
        // Do any additional setup after loading the view.
    }

    @IBAction func share(_ sender: Any) {
        Utils.share(vc: self, objectsToShare: [""])
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

}
