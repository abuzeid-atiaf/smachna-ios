//
//  CurrentOrdersViewController.swift
//  Smachna
//
//  Created by Mac on 12/8/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class CurrentOrdersViewController: BaseViewController  ,UITableViewDelegate ,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var homvc:String?
    var dataSource = [[String:AnyObject]]()
    private let cellIdentifier = "CurrentOrderTableCell"
    var  headerImageUrl = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavgationBar(hasMenu: true,title: nil, subTitle: nil, showSearch: true, back: false, cart: false)
        backHome = cids.rootNavigationCtrl
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "CurrentOrderTableCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        if app.isUserLoggedIn() {
            self.getDataFromServer()

        }else{
//        Toast.alert(vc:self ,message: "Sorry you arn't logged in , login first to get your order history".localized)
        }
        
        
        //"id":"2","name_ar":"\u0645\u0633\u062a\u0648\u0631\u062f","name_en":"exported","img":"831e0faccab44cda51ec935d78770869.jpg","products"
    }
    
    
    func getDataFromServer(){
        
        
        Progress.show(sender: self.view)
        
        
        let uid = app.getUserData()!.userid
        RequestManager.post(actionName: "home/client_orders", params: "user_id=\(uid)"  , respo: {(dictionary) -> Void in
            
            
            if let orders = dictionary["orders"] as?  [[String:AnyObject]] {
                
                app.setCurrentOrdersCount(count:"\(orders.count)")
                
                for object in orders {
                    
                    self.dataSource.append(object )
                    
                }
                
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
                Progress.hide()
            }
            
            
            
        })
    }
   
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell:CurrentOrderTableCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! CurrentOrderTableCell
        
        let current = dataSource[indexPath.row];
        
         cell.dataSource = dataSource[indexPath.row]["details"] as! [[String:AnyObject]]
        //  let name = app.englishLocale() ? "name_en":"name_ar"
        
        cell.totalPriceLbl.text = current["total_price"] as! String +  " " + "Dinar".localized
        cell.statusLbl.text  = app.orderStatus(st: current["status"]! as! String)
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let footerHeight = 60
        let rowHeight = 44
        
        let obj = dataSource[indexPath.row]["details"] as! [[String:AnyObject]]
        let num =  obj.count * rowHeight ;
        return CGFloat(num + footerHeight )
        
        
        
      //  return 120;
    }
    
    
    
    
}
