//
//  UsePolicyViewController.swift
//  Smachna
//
//  Created by Mac on 12/17/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class UsePolicyViewController: BaseViewController {
    
    ///START====================( STORYBOARD VIEWS )==============
    
    
    @IBOutlet weak var tv: UITextView!

    ///@END====================( STORYBOARD VIEWS )===============
    
    
    
    
    
    
    
    var aboutObj:[String:String]?
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavgationBar(hasMenu: true,title: nil, subTitle: nil, showSearch: true, back: true, cart: false)
        self.getDataFromServer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    
    func getDataFromServer(){
        
        
        Progress.show(sender: self.view)
        
        RequestManager.post(actionName: "home/about", params: "", respo: {(dictionary) -> Void in
            Progress.hide()
            
            
            
            DispatchQueue.main.async {
                
                if let about = dictionary["about"] as?  [String:AnyObject] {
                    if let aboutc = about["rules"] as? String {
                        self.tv.text = aboutc;
                        self.aboutObj = about as?  [String:String]
                        
                    }
                    
                }
            }
            
            
            
        })
    }

}
