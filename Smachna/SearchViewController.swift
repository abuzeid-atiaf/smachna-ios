//
//  SearchViewController.swift
//  Smachna
//
//  Created by Mac on 12/8/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit
//{"products":[],"validation":"Ops no result found","success":"false"}
class SearchViewController: BaseViewController ,UITableViewDelegate ,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var searchIcon: UIImageView!
    @IBOutlet weak var searchField: UITextField!
    
    
    
    
    @IBAction func searchChanged(_ sender: UITextField) {
        
        
        
        if sender.text!.characters.count > 0 {
            self.getDataFromServer(search: sender.text!)
            
            
        }

        
        
    }
    @IBAction func searchonChange(_ sender: UITextField) {
        
        if sender.text!.characters.count > 0 {
            self.getDataFromServer(search: sender.text!)

        
        }
        
        
        
        
        
        
    }
    
    @IBAction func searchAction(_ sender: Any) {
        
        self.getDataFromServer(search: self.searchField.text!)

        
    }
    
    
    var dataSource = [[String:AnyObject]]()
    private let cellIdentifier = "cell_search"
    var  headerImageUrl = ""
    
    
    
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavgationBar(hasMenu: true,title: nil, subTitle: nil, showSearch: false, back: true, cart: false)
        
        
        
        
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
//        self.tableView.register(UINib(nibName: "ImageTableCell", bundle: nil), forCellReuseIdentifier: "ImageTableCell")
        
    }
    
    
    /// {"offer":[{"id":"1","detail_ar":"\u0639\u0631\u0636 \u0639\u0631\u0636 \u0639\u0631\u0636 \u0639\u0631\u0636","detail_en":" offer offer offer offeroffer"}],"success":"true"}
    
    func getDataFromServer(search:String){
        
        
        Progress.show(sender: self.view)
        
        RequestManager.post(actionName: "home/search", params: "search=" + search, respo: {(dictionary) -> Void in
            Progress.hide()
            
            if let array = dictionary["products"] as?  [[String:AnyObject]] {
                self.dataSource.removeAll()
                for object in array {
                    
                    self.dataSource.append(object )
                    
                }
                
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
                
            }
            
            
            
        })
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    func itemClicked(sender: AnyObject) {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        let label:UILabel = cell?.viewWithTag(90) as! UILabel
        let key:String = app.englishLocale() ?  "name_en": "name_ar"
        label.text = self.dataSource[indexPath.row][key] as! String?
        
//        let url =  URL(string: constants.IMAGE_URL +  self.headerImageUrl);
//        cell.ctextLabel.text = app.productLocaleExported ? "Exported".localized : "Local".localized
//        cell.cImageView.kf.setImage(with: url)
//  
        
        
        return cell!
        
    }
    
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        
        
        
        
        let detail:ProductDetailsViewController = Cuts.vc(storyid: cids.ProductDetailsViewController) as! ProductDetailsViewController
            detail.productDic  = self.dataSource[indexPath.row]
        if let menu = sideMenuController {
            menu.embed(centerViewController: detail)
        }else{
        
        }
        
        // self.showVC(vc: detail)
        
        //sideMenuController!.embed(sideViewController: detail)
//self.present(detail, animated: true, completion: nil)
//        Cuts.present(vc: <#T##UIViewController#>, storyid: <#T##String#>)
    }
    

}
