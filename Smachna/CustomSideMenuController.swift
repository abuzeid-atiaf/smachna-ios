//
//  CustomSideMenuController.swift
//  Smachna
//
//  Created by Mac on 12/15/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit
import SideMenuController
class CustomSideMenuController: SideMenuController {

  
    required init?(coder aDecoder: NSCoder) {
        SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "menu")
        
        if app.englishLocale() {
           SideMenuController.preferences.drawing.sidePanelPosition = .overCenterPanelLeft
        }else{
        
        SideMenuController.preferences.drawing.sidePanelPosition = .overCenterPanelRight
        }
        
        
        
        
        SideMenuController.preferences.drawing.sidePanelWidth = 300
        SideMenuController.preferences.drawing.centerPanelShadow = true
        SideMenuController.preferences.animating.statusBarBehaviour = .showUnderlay
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

            }



}
