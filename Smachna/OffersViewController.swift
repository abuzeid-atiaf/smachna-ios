//
//  OffersViewController.swift
//  Smachna
//
//  Created by Mac on 12/11/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class OffersViewController: BaseViewController ,UITableViewDataSource,UITableViewDelegate {

    
    @IBOutlet weak var tableView: UITableView!
    var dataSource = [[String:AnyObject]]()
    private let cellIdentifier = "SingleLineTableCell"
    var  headerImageUrl = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavgationBar(hasMenu: true,title: nil, subTitle: nil, showSearch: true, back: false, cart: false)
        
        
        
        
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.reloadData()
        self.tableView.register(UINib(nibName: "SingleLineTableCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        self.getDataFromServer()
        
            }
    
    
    
    
    
    
    
    func getDataFromServer(){
        
        
        Progress.show(sender: self.view)
        
        RequestManager.post(actionName: "home/offer", params: "", respo: {(dictionary) -> Void in
             Progress.hide()
            
            if let array = dictionary["products"] as?  [[String:AnyObject]] {
              
                for object in array {
                    
                    self.dataSource.append(object )
                    
                }
                
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
               
            }
            
            
            
        })
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    func itemClicked(sender: AnyObject) {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
      func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:SingleLineTableCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SingleLineTableCell
        let name = app.englishLocale() ? "name_en":"name_ar"
        cell.titleLbl.text =  self.dataSource[indexPath.row][name] as! String?
        
        //let url =  URL(string: constants.IMAGE_URL +  self.headerImageUrl);
        
            //cell.cImageView.kf.setImage(with: url)
            return cell
        
    }
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        
        
       
            let detail:ProductDetailsViewController = Cuts.vc(storyid: cids.ProductDetailsViewController) as! ProductDetailsViewController
            
            detail.productDic  = self.dataSource[indexPath.row]
            
            
            
            self.showVC(vc: detail)
            
       
    }


}
