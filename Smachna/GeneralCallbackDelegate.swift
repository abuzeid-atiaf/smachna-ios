//
//  GeneralCallbackDelegate.swift
//  Smachna
//
//  Created by mac on 12/24/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import Foundation
protocol GeneralCallbackDelegate {
    
     func callbackObject(obj:AnyObject);
}
