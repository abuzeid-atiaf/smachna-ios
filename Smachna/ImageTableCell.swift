//
//  ImageTableCell.swift
//  Smachna
//
//  Created by Mac on 12/11/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class ImageTableCell: UITableViewCell {

    @IBOutlet weak var cImageView: UIImageView!
    
    @IBOutlet weak var ctextLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
