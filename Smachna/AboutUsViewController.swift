//
//  AboutUsViewController.swift
//  Smachna
//
//  Created by Mac on 12/8/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class AboutUsViewController: BaseViewController {
    
    
    ///START====================( STORYBOARD VIEWS )==============
    
    
    @IBOutlet weak var tv: UITextView!
    
    @IBAction func twitterAction(_ sender: Any) {
        Utils.openLink(url: self.aboutObj!["twpage"]!)
    }
    
    @IBAction func googleAction(_ sender: Any) {
        Utils.openLink(url: self.aboutObj!["googlepage"]!)
    }
    
    
    @IBAction func  fbAction(_ sender: Any) {
        
        Utils.openLink(url: self.aboutObj!["fbpage"]!)
    }
    
    
    @IBAction func linkedAction(_ sender: Any) {
        
        // Utils.openLink(url: self.aboutObj![""]!)
        
    }
    //,"":"https:\/\/m.facebook.com\/atiaf","":"https:\/\/twitter.com\/","instgram":"https:\/\/instgram.com\/","whatsapp":"1","":"https:\/\/google.com\/","wahtsapp":"https:\/\/www.whatsapp.com\/","address":"\u0627\u0644\u062c\u0627\u0628\u0631\u064a\u0629 \u06427 \u062e\u0644\u0641 \u0627\u0644\u0645\u062e\u0641\u0631\u200e","email":"info@domain.com","ads_img":nul
    ///@END====================( STORYBOARD VIEWS )===============
    
    
    
    
    
    
    
    var aboutObj:[String:String]?
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavgationBar(hasMenu: true,title: nil, subTitle: nil, showSearch: true, back: false, cart: false)
        self.getDataFromServer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    
    func getDataFromServer(){
        
        
        Progress.show(sender: self.view)
        
        RequestManager.post(actionName: "home/about", params: "", respo: {(dictionary) -> Void in
            Progress.hide()
            
            
            
            DispatchQueue.main.async {
                
                if let about = dictionary["about"] as?  [String:AnyObject] {
                    if let aboutc = about["about_company"] as? String {
                        self.tv.text = aboutc;
                        self.aboutObj = about as?  [String:String]
                        
                    }
                    
                }
            }
            
            
            
        })
    }
    
    
    
    
    
}
