//
//  OrderFormViewController.swift
//  Smachna
//
//  Created by Mac on 12/8/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit
class ContinueSignupViewController: BaseViewController
     ,UITextFieldDelegate
,UIPopoverPresentationControllerDelegate ,SpinnerItemSelectedProtocol {
   
    
    ///START====================( STORYBOARD VIEWS )==============
    var controllerTask:Int = 0;
    var showDetials = false
    var prameters:String =  ""
    var parentControllerID = ""
    private  let tag_region = 1
    private let tag_city = 9
    var tfTag = 0;
    
    var selectedRegId = "1"
    var selectedCityId = "1"
    var product:String?
    var price:String?
    var quantity:String?
    var totalprice:String?
    
    
    var regionArray=[[String:AnyObject]]();
    var citiesArray=[[String:AnyObject]]();
    
    
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var regionField: UITextField!
    @IBOutlet weak var steatField: UITextField!
    @IBOutlet weak var homeField: UITextField!
    @IBOutlet weak var mobileField: UITextField!
    @IBOutlet weak var jadaField: UITextField!
    
    
    
    @IBOutlet var scrollLayout: UIScrollView!
    
    @IBAction func registerButton(_ sender: AnyObject) {
        self.view.endEditing(true)
        
        if(regionField.text?.isEmpty)!{
            Toast.alert(vc:self ,message: "select your region".localized)
            return;
        }
        
        if(cityField.text?.isEmpty)!{
            Toast.alert(vc:self ,message: "select your city".localized)
            return;
            
        }
        
        
        
        if(controllerTask == constants.flag_task_signup  ){
            
            
            
            self.sendRegisterDataToServer()
        }else if( controllerTask == constants.flag_task_order ){
            
            if showDetials{
                showOrderDetails(sender:sender)
            }else{
            
                gotoPay(sender:sender)
            }
            
        }else if( controllerTask == constants.flag_task_edit_address){
            self.editAddress()
            
        }
        
        
        
    }
    
   
    
    func showOrderDetails(sender:AnyObject){
        if let pro = product{
            let title = pro
            let qnt = "Quantity".localized
            let pricest = "Price".localized
            let tot = "Total Price".localized
            let dod = "Delivery cost one dinar".localized
        
            let desc  = qnt + " : \( self.quantity! ) \n \(pricest) : \(self.price!) \n \(tot) : \(Float(self.price!)! * Float(self.quantity!)!) \n \(dod) "
            
            
            let alert = UIAlertController(title: title, message: desc, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Continue".localized, style: .default, handler: {(UIAlertAction)->Void in
                
                self.gotoPay(sender: sender)
                
                
                
            } ))
            alert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: {(UIAlertAction)->Void in
                
                self.dismiss(animated: true, completion: nil)
                
                
            } )
            )
            
            
            
            self.present(alert, animated: true, completion: nil)
            

        
        
        
        }
        
    }

    func gotoPay(sender:AnyObject){
    
    
        
        
        
        prameters += "&client_name=" + usernameField.text!
        prameters += "&client_mobile=" + mobileField.text!
        prameters += "&region_id=" + String(selectedRegId)
        prameters += "&city_id=" + selectedCityId
        prameters += "&street=s " + steatField.text!
        prameters += "&jada=" + jadaField.text!
        prameters += "&home=" + homeField.text!
        let vc:PaymentMethodViewController = Cuts.vc(storyid: cids.PaymentMethodViewController) as! PaymentMethodViewController
        
        vc.prams = prameters
        vc.modalPresentationStyle = .overCurrentContext //presentation style
        present(vc, animated: true, completion: nil)
        vc.popoverPresentationController?.sourceView = view
        let mv :UIView = sender as! UIView
        vc.popoverPresentationController?.sourceRect = mv.frame
        
        
        
    }
    ///@END====================( STORYBOARD VIEWS )===============
    
    func editAddress(){
        
        self.view.endEditing(true)
        
        prameters += "&first_name=" + usernameField.text!
        prameters += "&last_name=" + usernameField.text!
        prameters += "&jada=" + jadaField.text!//
        prameters += "&home=" + homeField.text!
        prameters += "&street=" + steatField.text!
        prameters += "&region_id=" + String(selectedRegId)
        prameters += "&city_id=" + selectedCityId
        prameters += "&user_id=" + app.getUserData()!.userid
        
        
        Progress.show(sender: self);
        RequestManager.post(actionName: "home/edit_address", params: prameters , respo: {(dictionary) -> Void in
            Progress.hide()
            if let userLogged = dictionary["success"]  {
                
                
                if ((userLogged as! String ) == "true"  ){
                    Toast.alert(vc: self, message: "Your address updated".localized)
                    if let userdic = dictionary["user"] as! [String : String]?{
                        app.saveUserData(user: app.userFromDic(data: userdic)!)
                        
                    }
                    self.view.isUserInteractionEnabled = false
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                        
                        Cuts.present(vc: self, storyid: cids.rootNavigationCtrl)
                    }
                    
                }else{
                    if let er =  dictionary["validation"]  as! [String:String]? {
                        
                        DispatchQueue.main.async{
                            if( er.first?.key == "mobile"){
                                
                                Toast.alert(vc: self, message: "the mobile number already registered".localized)
                            }else if (er.first?.key == "username"){
                                Toast.alert(vc: self, message: "the username already registered".localized)
                                
                            }else if (er.first?.key == "email"){
                                Toast.alert(vc: self, message: "the email already registered".localized)
                                
                                
                            }else{
                                
                                Toast.alert(vc: self, message: (er.first?.value)!)
                                
                            }
                        }
                        
                    }
                }
                
                
                
            }
        })
        
        
        
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var title = "Describe your address".localized
        hideKeypadWithTextFields()
        self.regionField.delegate = self;
        usernameField.delegate = self
        mobileField.delegate = self
        homeField.delegate = self
        jadaField.delegate = self
        steatField.delegate = self

        self.regionField.tag = tag_region
        self.cityField.delegate  = self
        self.cityField.tag = tag_city
                //self.cityField.inputView =
      
                    
                    
                    self.mobileField.delegate = self
        switch controllerTask {
        case constants.flag_task_edit_address:
            self.backHome = cids.settingViewController
            title = "Descripe your address".localized
            registerButton.setTitle("Update".localized, for: .normal)
            let user  = app.getUserData()
            usernameField.text = user!.username
            mobileField.text = user!.mobile
            homeField.text = user!.home
            jadaField.text = user!.jada
        steatField.text = user!.street
          break;
            
     
        case constants.flag_task_signup:
            usernameField.placeholder = "firstname".localized
            mobileField.placeholder = "lastname".localized
            mobileField.keyboardType = .default
            backHome = cids.dismiss
            break;
            
        case constants.flag_task_order:
            
            
            
            self.backHome = parentControllerID
            
            self.usernameField.placeholder = "Name".localized
            registerButton.setTitle("Order".localized, for: .normal)
            break;
            
            
        default:
            break;
        }
        
        self.setNavgationBar(hasMenu: false,title: title, subTitle: nil, showSearch: false, back: true, cart: false)
        
        
        self.getCitiesFromServer()
        
        self.scrollLayout.contentSize = CGSize(width: self.view.frame.size.width, height: 750 )
        
    }
    
    
    
    
    func itemSelected(selectedItem:[String:AnyObject]){
        
        if(tfTag == tag_region){
            
            self.selectedRegId = (selectedItem["id"] as! String)
            self.regionField.text = app.englishLocale() ? (selectedItem["name_en"] as! String):(selectedItem["name_ar"] as! String)
            
            self.getCitiesFromServer()
            
        }else if (tfTag == tag_city){
            
            self.regionArray = selectedItem["region"] as! [[String:AnyObject]]
            self.selectedCityId = (selectedItem["id"] as! String)
            self.cityField.text = app.englishLocale() ? (selectedItem["name_en"] as! String):(selectedItem["name_ar"] as! String)
            
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        //textField code
        
        textField.resignFirstResponder()  //if desired
        
        return true
    }
    func adaptivePresentationStyleForPresentationController(PC: UIPresentationController!) -> UIModalPresentationStyle {
        // This *forces* a popover to be displayed on the iPhone
        return .none
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tfTag = textField.tag
        if(tfTag == tag_city || tfTag == tag_region){
            self.addSetRegionsAdapter(sender: textField)
        }else{
            
        }
    }
    
    func addSetRegionsAdapter(sender:UITextField) {
        
        
        
        let popoverMenuViewController = SpinnerViewController()
        if tfTag == tag_city {
            popoverMenuViewController.dataSourceArray = self.citiesArray
            
        } else if tfTag == tag_region {
            popoverMenuViewController.dataSourceArray = self.regionArray
            
        }
        
        popoverMenuViewController.delegate = self;
        popoverMenuViewController.modalPresentationStyle = .overCurrentContext //presentation style
        
        
        present(popoverMenuViewController, animated: true, completion: nil)
        popoverMenuViewController.popoverPresentationController?.sourceView = sender
        popoverMenuViewController.popoverPresentationController?.sourceRect = sender.frame
        
        
        
    }
    
    func getCitiesFromServer(){
        
        
        Progress.show(sender: self);
        RequestManager.post(actionName: "home/city", params: ""  , respo: {(dictionary) -> Void in
            if let array = dictionary["city"] as?  [[String:AnyObject]] {
                for object in array {
                    self.citiesArray.append(object )
                    
                }
            }
            
            DispatchQueue.main.async{
            }
            
            
            
            Progress.hide()
            
        })
        
        
        
        
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    func sendRegisterDataToServer(){
        
        self.view.endEditing(true)
        
        
        prameters += "&first_name=" + usernameField.text!
        prameters += "&last_name=" + usernameField.text!
        prameters += "&region_id=" + String(selectedRegId)
        prameters += "&city_id=" + selectedCityId
        prameters += "&street=" + steatField.text!
        prameters += "&jada=" + jadaField.text!
        prameters += "&home=" + homeField.text!
        
        
        
        
        
        
        
        Progress.show(sender: self);
        RequestManager.post(actionName: "home/register", params: prameters , respo: {(dictionary) -> Void in
            Progress.hide()
            if let userLogged = dictionary["success"]  {
                
                
                if ((userLogged as! String ) == "true"  ){
                    
                    
                    Toast.alert(vc: self, message: "User added successfully".localized);
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2) ) {     Cuts.present(vc: self, storyid: cids.rootNavigationCtrl)      }
                }else{
                    if let er =  dictionary["validation"]  as! [String:String]? {
                        
                        DispatchQueue.main.async{
                            
                            if( er.first?.key == "mobile"){
                                
                                Toast.alert(vc: self, message: "the mobile number already registered".localized)
                            }else if (er.first?.key == "username"){
                                Toast.alert(vc: self, message: "the username already registered".localized)
                                
                            }else if (er.first?.key == "email"){
                                Toast.alert(vc: self, message: "the email already registered".localized)
                                
                                
                            }else{
                                
                                Toast.alert(vc: self, message: (er.first?.value)!)
                                
                            }
                        }
                        
                    }
                }
                
                
                
            }
        })
        
    }
    
    
    
}
