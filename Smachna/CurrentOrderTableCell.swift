//
//  CurrentOrderTableCell.swift
//  Smachna
//
//  Created by mac on 12/24/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class CurrentOrderTableCell: UITableViewCell ,UITableViewDelegate,UITableViewDataSource{

    
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var totalPriceLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    //=====
    var dataSource = [[String:AnyObject]](){
    
        didSet{
            
            tableView.delegate = self
            tableView.dataSource = self
            tableView.reloadData()
        
        
        }
    }
    private let cellIdentifier = "OrderItemTableCell"
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

    
        self.tableView.register(UINib(nibName: "OrderItemTableCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell:OrderItemTableCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! OrderItemTableCell
        
        let current = dataSource[indexPath.row]
                let name = app.englishLocale() ? "name_en":"name_ar"

        cell.nameLbl.text = current["product"]?[name] as? String
        cell.quantityLbl.text  = "Quantity".localized + ": \( current["quantity"] as! String )"
        
        
        
        cell.priceLbl.text  = current["price"] as! String + " " + "Dinar".localized
     //   cell.titleLbl.text =  self.dataSource[indexPath.row][name] as! String?
        return cell
        
        
    }
    
    
  
    
}
