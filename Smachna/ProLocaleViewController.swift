//
//  ProLocaleViewController.swift
//  Smachna
//
//  Created by Mac on 12/8/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class ProLocaleViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        if app.englishLocale(){
        startBtn.setBackgroundImage(#imageLiteral(resourceName: "export_left"), for: .normal)
       localBtn.setBackgroundImage(#imageLiteral(resourceName: "export_right"), for: .normal)
        }else{
            localBtn.setBackgroundImage(#imageLiteral(resourceName: "export_left"), for: .normal)
            startBtn.setBackgroundImage(#imageLiteral(resourceName: "export_right"), for: .normal)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func exportedac(_ sender: Any) {
        app.productLocaleExported = true;
        app.localeType = app.exported
    Cuts.present(vc: self, storyid: "AdsViewController")
        
    }
    
    @IBOutlet weak var startBtn: UIButton!
    @IBAction func local(_ sender: Any) {
        app.localeType = app.local
        app.productLocaleExported = false;
        Cuts.present(vc: self, storyid: "AdsViewController")
        
    }
    @IBOutlet weak var localBtn: UIButton!

}
