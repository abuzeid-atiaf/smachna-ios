//
//  ForgetPasswordViewController.swift
//  Smachna
//
//  Created by Mac on 12/29/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        self.view.isOpaque = false
        // Do any additional setup after loading the view.
        
        
        self.hideKeypadWithTextFields()
    }

    @IBOutlet weak var emailField: UITextField!
    @IBAction func send(_ sender: Any) {
        
        self.view.endEditing(true)
        if Utils.isValidEmail(emailStr: emailField.text! ){
            
            Progress.show(sender: self.view)
            let prm  = "email=\(emailField.text!)"
            RequestManager.post(actionName: "home/forgetpassword", params: prm, respo: {(dictionary) -> Void in
                Progress.hide()
                
                if let userLogged = dictionary["success"]  {
                    Toast.alert(vc: self, message: "your password will send to you email".localized)
                   
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                    
                    
                        Cuts.present(vc: self, storyid: cids.rootNavigationCtrl)

                    })
                    
                }else{
                
                
                if dictionary["validation"] != nil  {
                    
                    Toast.alert(vc:self ,message: "the user not found".localized)
                    
                }else{
                    Cuts.present(vc: self, storyid: cids.rootNavigationCtrl)

                    
                    }
                
                }
                
            })
        
        
        
        
        
        }else{
        
        Toast.alert(vc: self, message: "Enter correct email")
        }
        
        
    
    
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
