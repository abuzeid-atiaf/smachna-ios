//
//  AlertContinueViewController.swift
//  Smachna
//
//  Created by Mac on 12/27/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class AlertContinueViewController: BaseViewController {
    var parameters:String?
    
    var appearedOnce = true
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        self.view.isOpaque = false
        appearedOnce = false;
        if app.englishLocale() == false{
            tv.text = "x8v-9I-TSe.text".localized}
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(appearedOnce){
        self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        appearedOnce = true;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    
    
    
    }
    
    @IBOutlet weak var tv: UITextView!
    @IBAction func continueAction(_ sender: Any) {
        self.next()
    }
    func next(){
    
        let cont:ContinueSignupViewController = Cuts.vc(storyid: cids.continueSignupViewController) as! ContinueSignupViewController
        cont.controllerTask = constants.flag_task_signup
        cont.prameters = self.parameters!
        cont.parentControllerID = cids.registerController
    self.present(cont, animated: true, completion: nil)
    }
    
}
