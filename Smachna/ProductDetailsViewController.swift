//
//  ProductDetailsViewController.swift
//  Smachna
//
//  Created by Mac on 12/8/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class ProductDetailsViewController: BaseViewController ,GeneralCallbackDelegate , SpinnerItemSelectedProtocol{
    
    var imagesUrls = [String]()
    var qnt:Float = 0.0
    @IBOutlet weak var viewPager: ViewPager!
    @IBOutlet weak var proDescLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var qntLbl: UILabel!
    var extrasdataSource = [[String:AnyObject]]()
    var selectedExtra:[String:AnyObject]?
    
    @IBAction func minusAction(_ sender: AnyObject) {
        if qnt > 0 {
            qnt = qnt - 0.5
            qntLbl.text = String(qnt)
            
        }
    }
    
    @IBAction func plusAction(_ sender: AnyObject) {
        qnt = qnt + 0.5
        qntLbl.text = String(qnt)
        
    }
    
    
    
    
    
    
    func callbackObject( obj:AnyObject){
        
        
        //paymentMethod = obj as! String;
    }
    
    
    
    
    @IBAction func chooseextras(_ sender: Any) {
        showExtra()
    }
    
    
    
    func currentObject()->CartModel{
        
        let cart = CartModel()
        cart.id = self.productDic!["id"] as! String
        cart.name_ar = self.productDic!["name_ar"] as! String
        cart.name_en = self.productDic!["name_ar"] as! String
        cart.image = self.productDic!["img"] as! String
        cart.quantity = self.qntLbl.text!
        cart.price = self.productDic!["price"] as! String
        cart.extraid = selectedExtra!["id"] as! String
        cart.extra = self.nothingRadio.title(for: .normal)!
        return cart;
    }
    
    
    @IBAction func addtoCart(_ sender: Any) {
        
        self.addcart()
        
        
    }
    @IBAction func addToCart(_ sender: Any) {
        self.addcart()
    }
    
    func addcart(){
        
        
        if selectedExtra == nil {
            Toast.alert(vc:self,message: "Please choose extras".localized)
            return;
        }
        if qnt>0  {
            app.addItemToCart(item: currentObject(),quantity:qnt)
            Toast.showSuccess()
            Toast.alert(message: "Added to cart".localized)
            //showVC(vc: Cuts.vc(storyid: cids.CartViewController) as! BaseViewController)
        }else{
            //Toast.showFail()
            Toast.alert(vc:self ,message: "choose quantity".localized)
        }
        
        
    }
    @IBAction func buyAction(_ sender: AnyObject) {
        if selectedExtra == nil {

            Toast.alert(vc:self,message: "Please choose extras".localized)
            return;
        }
        
        
        if qnt>0 {
            
            
            self.nextOrderStep(sender:sender ,
                                     pro: Utils.orderJsonFromArray(dataSource:[currentObject()]))
        }else{
            //Toast.showFail()
            Toast.alert(vc:self,message: "choose quantity".localized)
        }
        
    }
    @IBOutlet weak var nothingRadio: UIButton!
    
    
    var productDic:[String:AnyObject]?
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let name = app.englishLocale() ? "name_en":"name_ar"
        self.setNavgationBar(hasMenu: true,title: nil, subTitle: self.productDic![name] as? String , showSearch: true, back: false, cart: true)
        imagesUrls.append(self.productDic!["img"] as!  String)
        
        viewPager.pageControl.pageIndicatorTintColor = UIColor.lightGray
         viewPager.pageControl.currentPageIndicatorTintColor =
            
            UIColor(red: CGFloat(19/255), green: CGFloat(63/255), blue: CGFloat(130/255), alpha: 1.0)
        
        
        if let imgs = self.productDic!["imgs"] {
            if let images = imgs as? [String] {
            for im in images  {
                imagesUrls.append(im)
                }}
        }
        let d = app.englishLocale() ? "description_en":"description_ar"
        proDescLbl.text = self.productDic![d] as? String
        
        priceLbl.text = "Price".localized + "  " +  (self.productDic!["price"] as? String)! + "  " + "Dinar".localized
        
        
        viewPager.dataSource = self;
        // Do any additional setup after loading the view, typically from a nib.
        viewPager.animationNext()
        
        getExtrasFromServer()
        
    }
    
    
    
    
    
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewPager.scrollToPage(index: 0)
    }
    
    
    
    
    func itemSelected(selectedItem:[String:AnyObject]){
        selectedExtra = selectedItem
        if let extras = app.englishLocale() ? (selectedItem["name_en"] as? String):(selectedItem["name_ar"] as? String){
            self.nothingRadio.setTitle(extras, for: .normal)
        
        }
        
        
    }
    
    func showExtra(){
    
        
        let popoverMenuViewController = SpinnerViewController()
            popoverMenuViewController.dataSourceArray = self.extrasdataSource
                popoverMenuViewController.delegate = self;
        popoverMenuViewController.modalPresentationStyle = .overCurrentContext //presentation style
        
        
        present(popoverMenuViewController, animated: true, completion: nil)
        
        
    
    }
    
    func getExtrasFromServer(){
        
        
        Progress.show(sender: self.view)
        
        RequestManager.post(actionName: "home/order_extra", params: "", respo: {(dictionary) -> Void in
            
            
            if let array = dictionary["extras"] as?  [[String:AnyObject]] {
                                for object in array {
                    
                    self.extrasdataSource.append(object )
                    
                }
                
            }
            DispatchQueue.main.async {
                Progress.hide()
            }
            
            
            
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func nextOrderStep(sender:Any , pro:String){
        
        var prameters = "total_price=" +  (self.productDic!["price"] as? String)!
        prameters +=  "&products=\(pro)"

        if app.isUserLoggedIn(){
        
        
            let user = app.getUserData()
            prameters += "&user_id=" + user!.userid
            
            prameters += "&client_name=" + user!.username
            prameters += "&client_mobile=" + user!.mobile
            prameters += "&region_id=" + user!.region_id
            prameters += "&city_id=" + user!.city_id
            prameters += "&street=s " + user!.street
            prameters += "&jada=" + user!.jada
            prameters += "&home=" + user!.home
        //gototpay
            
            
            let vc:PaymentMethodViewController = Cuts.vc(storyid: cids.PaymentMethodViewController) as! PaymentMethodViewController
            
            vc.prams = prameters
            vc.modalPresentationStyle = .overCurrentContext //presentation style
            present(vc, animated: true, completion: nil)
            vc.popoverPresentationController?.sourceView = view
            
            let mv :UIView = sender as! UIView
            vc.popoverPresentationController?.sourceRect = mv.frame
            

        }else{
        
            
            let cont:ContinueSignupViewController = Cuts.vc(storyid: cids.continueSignupViewController) as! ContinueSignupViewController
            cont.controllerTask = constants.flag_task_order
            cont.prameters = prameters
            cont.showDetials = true
            cont.product = "\(selectedExtra![app.englishLocale() ? "name_en" : "name_ar"]!)"
            cont.price = self.productDic!["price"]! as! String
            cont.quantity = self.qntLbl.text!
            
            cont.totalprice = self.priceLbl.text!
            cont.parentControllerID = cids.dismiss
            self.present(cont, animated: true, completion: nil)
        }
    
       
        
        
        
    }
    
    
    
    
    
}

















extension ProductDetailsViewController:ViewPagerDataSource{
    
    
    func numberOfItems(viewPager:ViewPager) -> Int {
        
        return self.imagesUrls.count;
    }
    
    func viewAtIndex(viewPager:ViewPager, index:Int, view:UIView?) -> UIView {
        var newView:ImageTableCell?;
        
        if(view == nil){
            newView =   (Bundle.main.loadNibNamed("ImageTableCell", owner: self, options: nil)?.first as? UIView as! ImageTableCell?)!
            newView!.ctextLabel.isHidden = true
        }else{
            
            newView = view as? ImageTableCell;
            
        }
        newView!.cImageView.kf.setImage(with: URL(string: constants.IMAGE_URL + imagesUrls[index]))
        
        
        return newView!
    }
    
    func didSelectedItem(index: Int) {
        print("select index \(index)")
    }
    
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}


extension UIColor {
    static func randomColor() -> UIColor {
        // If you wanted a random alpha, just create another
        // random number for that too.
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}
