//
//  OrderItemTableCell.swift
//  Smachna
//
//  Created by mac on 12/24/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class OrderItemTableCell: UITableViewCell {

    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
