//
//  HomeViewController.swift
//  Smachna
//
//  Created by Mac on 12/8/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class HomeViewController:  BaseViewController ,UITableViewDelegate ,UITableViewDataSource {
     @IBOutlet weak var tableView: UITableView!
    
    
    var dataSource = [[String:AnyObject]]()
    private let cellIdentifier = "SingleLineTableCell"
    var  headerImageUrl = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backHome = cids.ProLocaleViewController
        self.setNavgationBar(hasMenu: true,title: nil, subTitle: nil, showSearch: true, back: true, cart: false)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.reloadData()
        self.tableView.register(UINib(nibName: "ImageTableCell", bundle: nil), forCellReuseIdentifier: "ImageTableCell")
        self.tableView.register(UINib(nibName: "SingleLineTableCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getDataFromServer()

        
        
        
    }
    
    func getDataFromServer(){
        
        
         Progress.show(sender: self.view)
        
        RequestManager.post(actionName: "home/category", params: "", respo: {(dictionary) -> Void in
            
            
            if let array = dictionary["category"] as?  [[String:AnyObject]] {
                var newArr =  [[String:AnyObject]]()
                if app.productLocaleExported{
                    self.headerImageUrl = array[0]["img"] as! String
                newArr =  array[0]["products"] as! [[String : AnyObject]]
                
                }else{
                     self.headerImageUrl = array[1]["img"] as! String
                newArr =  array[1]["products"] as! [[String : AnyObject]]
                }
                for object in newArr {
                
                    self.dataSource.append(object )
                    
                }
                
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
                Progress.hide()
            }
            
            
            
        })
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.backHome = cids.ProLocaleViewController
    }
    
    
    func itemClicked(sender: AnyObject) {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count+1
    }
   
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell:ImageTableCell = tableView.dequeueReusableCell(withIdentifier: "ImageTableCell") as! ImageTableCell
            let url =  URL(string: constants.IMAGE_URL +  self.headerImageUrl);
            cell.ctextLabel.text = app.productLocaleExported ? "Imported".localized : "Local".localized
            cell.cImageView.kf.setImage(with: url)
            return cell
        
        }else{
            let cell:SingleLineTableCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SingleLineTableCell
            let name = app.englishLocale() ? "name_en":"name_ar"
            cell.titleLbl.text =  self.dataSource[indexPath.row-1][name] as! String?
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0  {
            return CGFloat(self.view.frame.size.height/4)
        }else{
            return CGFloat(44)}
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.row > 0 {
        let detail:ProductDetailsViewController = Cuts.vc(storyid: cids.ProductDetailsViewController) as! ProductDetailsViewController
        
            detail.productDic  = self.dataSource[indexPath.row-1]
        
        
        
        self.showVC(vc: detail)
        
        }
        
    }
    
    
}
