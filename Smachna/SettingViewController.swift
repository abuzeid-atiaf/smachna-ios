//
//  SettingViewController.swift
//  Smachna
//
//  Created by Mac on 12/15/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class SettingViewController: BaseViewController , UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    var dataSource = [String]()
    private let cellIdentifier = "cell_setting"
    var  headerImageUrl = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavgationBar(hasMenu: false,title: "settings".localized, subTitle: nil, showSearch: false, back: true, cart: false)
        
        
        self.dataSource.append("Edit Data".localized)
        self.dataSource.append("Edit Address".localized)
        self.dataSource.append("Change Language".localized)
        
        if app.isUserLoggedIn(){
        self.dataSource.append("Log Out".localized)
        }  else{
        self.dataSource.append("Login".localized)
        }
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.reloadData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    func itemClicked(sender: AnyObject) {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        
        let label:UILabel = cell?.viewWithTag(90) as! UILabel
        label.text = self.dataSource[indexPath.row]
        
        if indexPath.row == (dataSource.count - 1)  {
            label.textColor = UIColor.red
        }
        
        return cell!
        
    }
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
            
        case 0://edit data
            
            if app.isUserLoggedIn() {
            let vc:EditProfileViewController =  Cuts.vc(storyid: cids.EditProfileViewController) as! EditProfileViewController
                
           
            self.present(vc, animated: true, completion: nil)
            
            }
                break;
        //edit address
        case 1:
            if app.isUserLoggedIn() {

            let vc:ContinueSignupViewController =  Cuts.vc(storyid: cids.continueSignupViewController) as! ContinueSignupViewController
           
                vc.controllerTask  = constants.flag_task_edit_address
                
            self.present(vc, animated: true, completion: nil)
            }
            break;
        case 2:
//            Toast.alert(vc:self ,message: "its prefered to restart the app to reset locale".localized)
//            let current = app.getLocale();
//            var nextLocale = ""
//            if  current == constants.arabicCode {
//                nextLocale = constants.englishCode
//            }else{
//                nextLocale = constants.arabicCode
//                
//                
//            }
//            
//            LanguageManager.sharedInstance.setLocale(nextLocale)
//            Cuts.present(vc: self, storyid: cids.rootNavigationCtrl );
//            

            
            Cuts.present(vc: self, storyid: "ChangeLangViewController")
           // ChangeLangViewController
            
            break;
            
        case 3:
            
            if app.isUserLoggedIn(){
            app.removeUserData()
            Cuts.present(vc: self, storyid: cids.rootNavigationCtrl );
            }else{
                Cuts.present(vc: self, storyid: cids.loginController );

            
            }
            
            
            break;
        default:
            
            break;
        }
        
    }
    
    
    func changeLanguageAlert(){
        
        
        
    }
}
