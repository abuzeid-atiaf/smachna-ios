//
//  SingleLineTableCell.swift
//  Smachna
//
//  Created by Mac on 12/6/16.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class SingleLineTableCell: UITableViewCell {

    @IBOutlet weak var arrow: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if(app.englishLocale()){
            arrow.image = UIImage(named:"arrow_left")
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
